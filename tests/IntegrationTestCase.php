<?php

namespace Tests;

use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;

abstract class IntegrationTestCase extends TestCase
{
    use DatabaseTransactions;

    protected string $token;

    protected array $recruiterUserOwner;

    protected string $clientId = "92cb3a21-1e80-4e2c-8a24-ca7f4bf4065f";

    /**
     * Set up the test case.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate');

        $this->recruiterUserOwner = $this->createRecruiterAdminUser();
        $this->token = $this->getAuthToken($this->recruiterUserOwner['user']);
    }

    /**
     * Create a recruiter user
     *
     * @return array
     */
    public function createRecruiterAdminUser(): array
    {
        $user = User::factory()->count(1)->create()->first();
        $organization = Organization::factory()->count(1)->create([
            'user_id' => $user->id
        ])->first();
        $recruiterDetail = RecruiterUserDetail::factory()->count(1)->create([
            'user_id' => $user->id,
            'organisation_id' => $organization->id
        ]);
        $user->recruiterDetail = $recruiterDetail;
        return [
            'user' => $user,
            'organization' => $organization,
        ];
    }

    /**
     * Create a recruiter user
     *
     * @return array
     */
    public function createRecruiterStandardUser(): array
    {
        $user = User::factory()->count(1)->create()->first();
        $organization = Organization::factory()->count(1)->create([
            'user_id' => $user->id
        ])->first();
        $recruiterDetail = RecruiterUserDetail::factory()->count(1)->create([
            'user_id' => $user->id,
            'organisation_id' => $organization->id,
            'permissions_level' => 'Standard'
        ]);
        $user->recruiterDetail = $recruiterDetail;
        return [
            'user' => $user,
            'organization' => $organization,
        ];
    }

    /**
     * Gets auth token from auth service
     *
     * @param User|null $user
     *
     * @return string
     */
    public function getAuthToken(User $user = null): string
    {
        if (!$user) {
            $user = User::factory()->count(1)->create()->first();
        }
        $authUrl = config('internal.auth.base_url');
        $url = "$authUrl/v1/credentials";
        $postBody = [
            "client_id" => $this->clientId,
            "user_id" => $user->id,
            "username" => $user->email,
            "password" => $user->password,
            "password_confirmation" => $user->password
        ];
        $response = Http::post($url, $postBody);
        $responseBody = $response->json();
        return $responseBody['access_token'];
    }
}
