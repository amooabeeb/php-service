<?php

namespace Tests\Feature;

use Tests\TestCase;

class HealthTest extends TestCase
{

    /**
     * Test service's health
     *
     * @return void
     */
    public function testHealthCheck()
    {
        $response = $this->json(
            'GET',
            "/v1/healthCheck"
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('status', $decodedResponse);
    }

    /**
     * Test for 404 routes
     *
     * @return void
     */
    public function test404()
    {
        $response = $this->json(
            'GET',
            "/v1/hueuwywy"
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(404);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('errors', $decodedResponse);
    }
}
