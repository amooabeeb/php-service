<?php

namespace Tests\Feature;

use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use Tests\IntegrationTestCase;
use Tests\TestCase;

class OrganizationTest extends IntegrationTestCase
{

    /**
     * Success test to fetch an organization's detail
     *
     * @return void
     */
    public function testFetchOrganizationDetailsSuccess()
    {
        $user = User::factory()->count(1)->create()->first();
        $organization = Organization::factory()->count(1)->create([
            'user_id' => $user->id
        ])->first();
        RecruiterUserDetail::factory()->count(1)->create([
           'user_id' => $user->id,
           'organisation_id' => $organization->id
        ]);
        $token = $this->getAuthToken($user);
        $response = $this->json(
            'GET',
            "/v1/organizations/$organization->id?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('id', $decodedResponse['data']);
        $this->assertArrayHasKey('industry', $decodedResponse['data']);
    }

    /**
     * Failure test to get organization details
     *
     * @return void
     */
    public function testFetchOrganizationDetailsFailure()
    {
        $response = $this->json(
            'GET',
            "/v1/organizations/0?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(400);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('title', $decodedResponse['errors'][0]);
    }
}
