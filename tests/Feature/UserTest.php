<?php

namespace Tests\Feature;

use App\Common\Errors\UserError;
use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use App\Models\UserGoogle;
use App\Models\UserLinkedin;
use Tests\IntegrationTestCase;

class UserTest extends IntegrationTestCase
{

    /**
     *  Success test to fetch an user's detail
     *
     * @return void
     */
    public function testFetchUserDetailsSuccess()
    {
        $user = User::factory()->count(1)->create()->first();
        $token = $this->getAuthToken($user);
        $response = $this->json(
            'GET',
            "/v1/users/$user->id?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('id', $decodedResponse['data']);
    }

    /**
     * Failure test to fetch a user's details
     *
     * @return void
     */
    public function testFetchUserDetailsFailure()
    {
        $response = $this->json(
            'GET',
            "/v1/users/0?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(400);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test fetch user timezones
     *
     * @return void
     */
    public function testFetchTimezones()
    {
        $response = $this->json(
            'GET',
            "/v1/timezones?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
    }

    /**
     * Test update user profile success
     *
     * @return void
     */
    public function testUpdateUserProfileSuccess()
    {
        $user = User::factory()->count(1)->create()->first();
        $token = $this->getAuthToken($user);
        $response = $this->json(
            'PATCH',
            "/v1/users/$user->id",
            ['client_id' => $this->clientId, 'user_timezone' => 'Africa/Ghana'],
            ['Authorization' => "Bearer $token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
        $this->assertEquals('Africa/Ghana', $decodedResponse['data']['user_timezone']);
    }

    /**
     * Test update user profile error
     *
     * @return void
     */
    public function testUpdateUserProfileError()
    {
        $user = User::factory()->count(1)->create()->first();
        $response = $this->json(
            'PATCH',
            "/v1/users/$user->id",
            ['client_id' => $this->clientId, 'user_timezone' => 'Africa/Ghana'],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(403);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test get permissions for user with permissions
     *
     * @return void
     */
    public function testItReturnsPermissionForUserWithPermission()
    {
        $user = User::factory()->count(1)->create()->first();
        $organization = Organization::factory()->count(1)->create([
            'user_id' => $user->id
        ])->first();
        RecruiterUserDetail::factory()->count(1)->create([
            'user_id' => $user->id,
            'organisation_id' => $organization->id,
        ]);
        $response = $this->json(
            'GET',
            "/v1/users/$user->id/permissions"
        );
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonStructure([
            'data' => [
                'user_type',
                'user_permissions'
            ]
        ]);
    }

    /**
     * Test return null for user with permissions
     *
     * @return void
     */
    public function testItReturnsNullForUserWithoutPermission()
    {
        $user = User::factory()->count(1)->create()->first();
        $response = $this->json(
            'GET',
            "/v1/users/$user->id/permissions"
        );
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonStructure([
            'data' => [
                'user_type',
                'user_permissions'
            ]
        ]);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertNull($decodedResponse['data']['user_permissions']);
    }

    /**
     * Test return google auth profile for valid user
     *
     * @return void
     */
    public function testItReturnsGoogleAuthProfileForValidUser()
    {
        $userGoogle = UserGoogle::factory()->count(1)->create()->first();
        $response = $this->json(
            'GET',
            "/v1/users/google/$userGoogle->?client_id=$this->clientId",
        );
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonStructure([
            'data' => [
                'user_id'
            ]
        ]);
    }

    /**
     * Test return linkedin auth profile for valid user
     *
     * @return void
     */
    public function testItReturnsLinkedInAuthProfileForValidUser()
    {
        $userLinkedin = UserLinkedin::factory()->count(1)->create()->first();
        $response = $this->json(
            'GET',
            "/v1/users/linkedin/$userLinkedin->?client_id=$this->clientId",
        );
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonStructure([
            'data' => [
                'user_id'
            ]
        ]);
    }

    /**
     * Test return linkedin auth profile for valid user
     *
     * @return void
     */
    public function testItReturnsSocialAuthErrorForInValidUser()
    {
        $userLinkedin = UserLinkedin::factory()->count(1)->create()->first();
        $response = $this->json(
            'GET',
            "/v1/users/google/$userLinkedin->?client_id=$this->clientId",
        );
        $response->assertStatus(422);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonStructure([
            'errors' => [UserError::USER_NOT_FOUND]
        ]);
    }
}
