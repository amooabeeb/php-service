<?php

namespace Tests\Feature;

use App\Models\OrganizationGroup;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Testing\TestResponse;
use Tests\IntegrationTestCase;

class OrganizationGroupTest extends IntegrationTestCase
{
    /**
     * Test that an admin recruiter can create a group
     *
     * @return void
     */
    public function testRecruiterAdminCanCreateAGroup()
    {
        $this->createOrganizationGroup($this->token);
    }

    /**
     * Call create organization group endpoint
     *
     * @param string $token
     *
     * @return TestResponse
     */
    private function createOrganizationGroup(string $token): TestResponse
    {
        $response = $this->json(
            'POST',
            '/v1/groups',
            ['client_id' => $this->clientId],
            ['Authorization' => "Bearer $token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);

        return $response;
    }

    /**
     * Test that default group names are generated per organization
     *
     * @return void
     */
    public function testGenerateDefaultGroupNameByOrganization()
    {
        $recruiterUser2 = $this->createRecruiterAdminUser();
        $token2 = $this->getAuthToken($recruiterUser2['user']);
        $this->createOrganizationGroup($this->token);
        $this->createOrganizationGroup($this->token);
        $recruiterResponse2 = $this->createOrganizationGroup($token2);
        $decodedRecruiterResponse2 = json_decode($recruiterResponse2->getContent(), true);
        $group = $decodedRecruiterResponse2['data'][0];
        $this->assertArrayHasKey('name', $group);
        $this->assertEqualsIgnoringCase('Group 1', $group['name']);
    }

    /**
     * Test that an admin recruiter can view list of groups
     *
     * @return void
     */
    public function testRecruiterAdminCanViewGroups()
    {
        OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ]);
        $response = $this->json(
            'GET',
            "/v1/groups?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
    }

    /**
     * Test that an admin recruiter can delete group
     *
     * @return void
     */
    public function testRecruiterAdminCanDeleteGroup()
    {
        $organizationGroup = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ])->first();
        $response = $this->json(
            'DELETE',
            "/v1/groups/$organizationGroup->id?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertEmpty($decodedResponse['data']);
    }

    /**
     * Test that an admin recruiter can update group
     *
     * @return void
     */
    public function testRecruiterAdminCanUpdateGroup()
    {
        $organizationGroup = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ])->first();
        $response = $this->json(
            'PATCH',
            "/v1/groups/$organizationGroup->id?client_id=$this->clientId",
            ['name' => 'Groupie'],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
        $this->assertEquals('Groupie', $decodedResponse['data'][0]['name']);
    }

    /**
     * Test cannot update group with an existing name
     *
     * @return void
     */
    public function testRecruiterAdminCannotUpdateGroupWithAnExistingName()
    {
        OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ]);
        $organizationGroup2 = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id,
            'name' => 'Group 2'
        ])->first();
        $response = $this->json(
            'PATCH',
            "/v1/groups/$organizationGroup2->id",
            ['name' => 'Group 1', 'client_id' => $this->clientId],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(422);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test returns 400 for invalid group
     *
     * @return void
     */
    public function testReturnsAppropriateErrorForInvalidGroup()
    {
        $organizationGroup = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ])->first();
        $response = $this->json(
            'PATCH',
            "/v1/groups/" . ($organizationGroup->id + 1),
            ['name' => 'Group 1', 'client_id' => $this->clientId],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(400);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test returns 403 for unauthorized organization group
     *
     * @return void
     */
    public function testReturnsAppropriateErrorForUnAuthorizedOrganizationGroup()
    {
        $organizationGroup = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id + 1
        ])->first();
        $response = $this->json(
            'PATCH',
            "/v1/groups/$organizationGroup->id",
            ['name' => 'Group 1', 'client_id' => $this->clientId],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(403);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test returns 403 for unauthorized user
     *
     * @return void
     */
    public function testReturnsAppropriateErrorForUnAuthorizedUser()
    {
        $recruiterUser = $this->createRecruiterStandardUser();
        $token = $this->getAuthToken($recruiterUser['user']);
        $response = $this->json(
            'PATCH',
            '/v1/groups/1',
            ['name' => 'Group 1', 'client_id' => $this->clientId],
            ['Authorization' => "Bearer $token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(403);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test add user to group
     *
     * @return void
     */
    public function testAddUserToGroup()
    {
        $user = $this->createRecruiterStandardUser();
        $organizationGroup = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ])->first();
        $response = $this->json(
            'POST',
            "/v1/groups/$organizationGroup->id/users",
            ['user_id' => $user['user']->id, 'client_id' => $this->clientId],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertArrayHasKey('data', $decodedResponse);
        $organizationResult = $decodedResponse['data'][0];
        $this->assertArrayHasKey('users', $organizationResult);
        $this->assertGreaterThan(0, sizeof($organizationResult['users']));
    }

    /**
     * Test detach user from group
     *
     * @return void
     */
    public function testDetachUserFromGroup()
    {
        $user = User::factory()->count(1)->create()->first();
        $organizationGroup = OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ])->each(function ($group) use ($user) {
            $group->users()->save($user);
        })->first();
        $response = $this->json(
            'DELETE',
            "/v1/groups/$organizationGroup->id/users/$user->id",
            ['client_id' => $this->clientId],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertArrayHasKey('data', $decodedResponse);
        $organizationResult = $decodedResponse['data'][0];
        $this->assertArrayHasKey('users', $organizationResult);
        $this->assertEquals(0, sizeof($organizationResult['users']));
    }

    /**
     * Test get groups to which a user belongs
     *
     * @return void
     */
    public function testGetUserGroups()
    {
        $user = User::factory()->count(1)->create()->first();
        RecruiterUserDetail::factory()->count(1)->create([
            'user_id' => $user->id,
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ]);
        OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ])->each(function ($group) use ($user) {
            $group->users()->save($user);
        });
        OrganizationGroup::factory()->count(1)->create([
            'organisation_id' => $this->recruiterUserOwner['organization']->id
        ]);
        $response = $this->json(
            'GET',
            "/v1/users/$user->id/groups?client_id=$this->clientId",
            [],
            ['Authorization' => "Bearer $this->token"]
        );
        $decodedResponse = json_decode($response->getContent(), true);
        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $this->assertArrayNotHasKey('errors', $decodedResponse);
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('organisation_id', $decodedResponse['data']);
        $this->assertArrayHasKey('groups', $decodedResponse['data']);
        $this->assertEquals(1, sizeof($decodedResponse['data']['groups']));
    }
}
