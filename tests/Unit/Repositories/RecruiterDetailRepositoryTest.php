<?php

namespace Tests\Unit\Repositories;

use App\Models\RecruiterUserDetail;
use App\Repositories\Concrete\RecruiterDetailRepository;
use Illuminate\Database\Eloquent\Collection;
use Mockery;
use Tests\TestCase;

class RecruiterDetailRepositoryTest extends TestCase
{
    /**
     * Mocked Recruiter Model
     */
    protected $recruiterMock;

    /**
     * Mocked Recruiter Repository
     *
     * @var mixed
     */
    protected $recruiterRepository;

    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->recruiterMock = Mockery::mock(RecruiterUserDetail::class);
        $this->recruiterMock->shouldReceive('getAttribute')->with('id')->andReturn(1);
        $this->app->instance(RecruiterUserDetail::class, $this->recruiterMock);
        $this->recruiterRepository = $this->app->make(RecruiterDetailRepository::class);
    }

    /**
     * Test that we can get recruiters for an organization
     *
     * @return void
     */
    public function testCanGetRecruitersForAnOrganization(): void
    {
        $this->recruiterMock->shouldReceive('where')->once()->with('organisation_id', 1)->andReturnSelf();
        $this->recruiterMock->shouldReceive('get')->once()->andReturn(new Collection([$this->recruiterMock]));
        $recruiters = $this->recruiterRepository->getRecruitersForOrganization(1);
        $this->assertInstanceOf(Collection::class, $recruiters);
        $this->assertNotEmpty($recruiters);
        $this->assertEquals(1, $recruiters[0]->id);
    }

    /**
     * Test that we can get recruiters for an organization
     *
     * @return void
     */
    public function testRecruiterCanAccessOrganization(): void
    {
        $this->recruiterMock->shouldReceive('getAttribute')->with('user_id')->andReturn(7);
        $recruiters = Collection::make([$this->recruiterMock]);
        $hasAccess = $this->recruiterRepository->canAccessOrganization($recruiters, 7);
        $noAccess = $this->recruiterRepository->canAccessOrganization($recruiters, 5);
        $this->assertEquals(true, $hasAccess);
        $this->assertEquals(false, $noAccess);
    }
}
