<?php

namespace Tests\Unit\Repositories;

use App\Models\Industry;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use App\Repositories\Concrete\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\App;
use Mockery;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    /**
     * Mocked User Model
     */
    protected $userMock;

    /**
     * Mocked RecruiterDetail Model
     */
    protected $recruiterDetailMock;

    /**
     * Mocked User Repository
     *
     * @var mixed
     */
    protected $userRepository;

    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->userMock = Mockery::mock(User::class);
        $this->recruiterDetailMock = Mockery::mock(RecruiterUserDetail::class);
        $this->userMock->shouldReceive('getAttribute')->with('id')->andReturn(1);
        $this->app->instance(User::class, $this->userMock);
        $this->userRepository = $this->app->make(UserRepository::class);
    }

    /**
     *  Tests that we can get user by Id
     *
     * @return void
     */
    public function testCanGetUserById(): void
    {
        $this->userMock->shouldReceive('where')->with('id', 1)->andReturnSelf();
        $this->userMock->shouldReceive('with')->with('recruiterDetail')->andReturnSelf();
        $this->userMock->shouldReceive('first')->andReturnSelf();
        $user = $this->userRepository->getById(1);
        $this->assertInstanceOf(User::class, $user);
        $this->assertNotNull($user);
        $this->assertEquals(1, $user->id);
    }

    /**
     *  Test user can get all timezones
     *
     * @return void
     */
    public function testCanGetAllTimezones(): void
    {
        $timezones = $this->userRepository->getAllTimezones();
        $this->assertIsArray($timezones);
        $this->assertNotEmpty($timezones);
    }

    /**
     * Test whether a requester can access a user's details
     *
     * @return void
     */
    public function testCanAccessUserDetails(): void
    {
        $hasAccess = $this->userRepository->canAccessUserDetails(1, 1);
        $forbiddenAccess = $this->userRepository->canAccessUserDetails(1, 2);
        $this->assertIsBool($hasAccess);
        $this->assertIsBool($forbiddenAccess);
        $this->assertEquals(true, $hasAccess);
        $this->assertEquals(false, $forbiddenAccess);
    }

    /**
     * Test user can update details
     *
     * @return void
     */
    public function testCanUpdateUserDetails(): void
    {
        $this->userMock->shouldReceive('where')->with('id', 1)->andReturnSelf();
        $this->userMock->shouldReceive('update')->with([])->andReturnTrue();
        $this->userMock->shouldReceive('with')->with('recruiterDetail')->andReturnSelf();
        $this->userMock->shouldReceive('first')->andReturnSelf();
        $updatedUser = $this->userRepository->updateUserDetails(1, []);
        $this->assertInstanceOf(User::class, $updatedUser);
        $this->assertNotEmpty($updatedUser);
    }

    /**
     * Test it can get organization users
     *
     * @return void
     */
    public function testGetOrganizationUsers(): void
    {
        $this->recruiterDetailMock->shouldReceive('getAttribute')->with('organisation_id')->andReturn(5);
        $this->userMock->shouldReceive('getAttribute')->with('recruiter_detail')->andReturn($this->recruiterDetailMock);
        $this->userMock->shouldReceive('whereHas')
            ->with('recruiterDetail', Mockery::on(function ($query) {
                $mockQueryBuilder = Mockery::mock(Builder::class);
                $mockQueryBuilder->shouldReceive('where')->with('organisation_id', 5)->andReturn($mockQueryBuilder);
                $query($mockQueryBuilder);
                return true;
            }))->andReturnSelf();
        $this->userMock->shouldReceive('with')->with('recruiterDetail')->andReturnSelf();
        $this->userMock->shouldReceive('get')->andReturn(Collection::make([$this->userMock]));
        $organizationUsers = $this->userRepository->getOrganizationUsers(5);
        $this->assertNotEmpty($organizationUsers);
        $this->assertEquals(5, $organizationUsers[0]->recruiter_detail->organisation_id);
    }

    /**
     * Test it can return user profile with permissions
     *
     * @return void
     */
    public function testGetByIdWithProfileReturnsUserProfileWithPermissions(): void
    {
        $this->recruiterDetailMock
            ->shouldReceive('getAttribute')
            ->once()->with('permissions_level')
            ->andReturn('Owner');
        $this->userMock
            ->shouldReceive('getAttribute')
            ->with('recruiter_detail')
            ->andReturn($this->recruiterDetailMock);
        $this->userMock->shouldReceive('withProfileDetail')->andReturnSelf();
        $this->userMock->shouldReceive('where')->with('id', 1)->andReturnSelf();
        $this->userMock->shouldReceive('first')->andReturnSelf();
        $user = $this->userRepository->getByIdWithProfile(1);
        $this->assertNotNull($user);
        $this->assertNotNull($user->recruiter_detail);
        $this->assertEquals('Owner', $user->recruiter_detail->permissions_level);

        $this->recruiterDetailMock->shouldReceive('getAttribute')->once()->with('permissions_level')->andReturn(null);
        $this->assertNull($user->recruiter_detail->permissions_level);
    }

    /**
     *  Tear down thee test case
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
