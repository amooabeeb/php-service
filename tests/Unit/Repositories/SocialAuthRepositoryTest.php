<?php

namespace Tests\Unit\Repositories;

use App\Models\User;
use App\Models\UserGoogle;
use App\Models\UserLinkedin;
use App\Repositories\Concrete\SocialAuthRepository;
use Mockery;
use Tests\TestCase;

class SocialAuthRepositoryTest extends TestCase
{
    /**
     * Mocked Google user Model
     */
    protected $userGoogleMock;

    /**
     * Mocked Linkedin user Model
     */
    protected $userLinkedinMock;

    /**
     * Mocked SocialAuth Repository
     *
     * @var mixed
     */
    protected $socialAuthRepository;

    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->userGoogleMock = Mockery::mock(UserGoogle::class);
        $this->userLinkedinMock = Mockery::mock(UserLinkedin::class);
        $this->app->instance(UserGoogle::class, $this->userGoogleMock);
        $this->app->instance(UserLinkedin::class, $this->userLinkedinMock);
        $this->userGoogleMock->shouldReceive('getAttribute')->with('user_id')->andReturn(5);
        $this->userLinkedinMock->shouldReceive('getAttribute')->with('user_id')->andReturn(5);
        $this->socialAuthRepository = $this->app->make(SocialAuthRepository::class);
    }

    /**
     *  Test it returns instance of Google user
     *
     * @return void
     */
    public function testItReturnsCorrectInstanceofGoogleUser(): void
    {
        $driver = $this->socialAuthRepository->getDriver('google');
        $this->assertInstanceOf(UserGoogle::class, $driver);
    }

    /**
     *  Test it returns instance of Linkedin user
     *
     * @return void
     */
    public function testItReturnsCorrectInstanceofLinkedinUser(): void
    {
        $driver = $this->socialAuthRepository->getDriver('linkedin');
        $this->assertInstanceOf(UserLinkedin::class, $driver);
    }

    /**
     *  Test it returns social auth profile
     *
     * @return void
     */
    public function testItReturnsSocialAuthProfile(): void
    {
        $this->userGoogleMock->shouldReceive('where')->with('socialite_id', 5)->andReturnSelf();
        $this->userGoogleMock->shouldReceive('first')->andReturnSelf();
        $socialProfile = $this->socialAuthRepository->getSocialAuthBySocialiteId(5, 'google');
        $this->assertInstanceOf(UserGoogle::class, $socialProfile);
    }

    /**
     *  Tear down thee test case
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
