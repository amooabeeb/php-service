<?php

namespace Tests\Unit\Repositories;

use App\Models\OrganizationGroup;
use App\Repositories\Concrete\OrganizationGroupRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;
use Mockery;
use Tests\TestCase;

class OrganizationGroupRepositoryTest extends TestCase
{
    /**
     * Mocked OrganizationGroupTest Model
     */
    protected $organizationGroupMock;

    /**
     * Instance of OrganizationGroupRepository
     */
    protected OrganizationGroupRepository $organizationGroupRepository;

    /**
     * Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->organizationGroupMock = $this->mock(OrganizationGroup::class);
        $this->organizationGroupRepository = $this->app->make(OrganizationGroupRepository::class);
    }

    /**
     * Test get group by Id
     *
     * @return void
     */
    public function testGetGroupById(): void
    {
        $this->organizationGroupMock->shouldReceive('find')->once()->with(1)->andReturnSelf();
        $group = $this->organizationGroupRepository->getGroupById(1);
        $this->assertNotNull($group);
    }

    /**
     * Test delete user from Group
     *
     * @return void
     */
    public function testDeleteUserFromGroup(): void
    {
        $relationShip = $this->mock(BelongsToMany::class);
        $relationShip->shouldReceive('detach')->once()->with(5)->andReturn(true);
        $this->organizationGroupMock->shouldReceive('where')->once()->with('id', 1)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('first')->once()->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('users')->once()->andReturn($relationShip);
        $groupDeleted = $this->organizationGroupRepository->deleteUserFromGroup(1, 5);
        $this->assertTrue($groupDeleted);
    }

    /**
     * Test add user to Group when user already exists
     *
     * @return void
     */
    public function testAddUserToGroupWhenUserAlreadyExists(): void
    {
        $relationShip = $this->mock(BelongsToMany::class);
        $relationShip->shouldReceive('where')->once()->with('users.id', 5)->andReturnSelf();
        $relationShip->shouldReceive('exists')->once()->andReturn(true);
        $this->organizationGroupMock->shouldReceive('where')->once()->with('id', 1)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('first')->once()->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('users')->once()->andReturn($relationShip);
        $groupDeleted = $this->organizationGroupRepository->addUserToGroup(1, 5);
        $this->assertTrue($groupDeleted);
    }

    /**
     * Test add user to Group when user does not exist
     *
     * @return void
     */
    public function testAddUserToGroupWhenUserDoesNotExist(): void
    {
        $relationShip = $this->mock(BelongsToMany::class);
        $relationShip->shouldReceive('where')->once()->with('users.id', 5)->andReturnSelf();
        $relationShip->shouldReceive('exists')->once()->andReturn(false);
        $relationShip->shouldReceive('attach')->once()->with(5)->andReturn(true);
        $this->organizationGroupMock->shouldReceive('where')->once()->with('id', 1)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('first')->once()->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('users')->twice()->andReturn($relationShip);
        $groupDeleted = $this->organizationGroupRepository->addUserToGroup(1, 5);
        $this->assertTrue($groupDeleted);
    }

    /**
     * Test update group with users
     *
     * @return void
     */
    public function testUpdateGroupWithUsers(): void
    {
        $this->organizationGroupMock->shouldReceive('where')->once()->with('id', 1)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('first')->once()->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('setAttribute')->once()->with('name', 'Group 2')->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('save')->once()->andReturnSelf();
        $updatedGroup = $this->organizationGroupRepository->updateGroupWithUsers(1, ['name' => 'Group 2']);
        $this->assertInstanceOf(OrganizationGroup::class, $updatedGroup);
    }

    /**
     * Test delete group
     *
     * @return void
     */
    public function testDeleteGroup(): void
    {
        $relationShip = $this->mock(BelongsToMany::class);
        $relationShip->shouldReceive('detach')->once();
        $this->organizationGroupMock->shouldReceive('where')->once()->with('id', 1)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('first')->once()->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('users')->once()->andReturn($relationShip);
        $this->organizationGroupMock->shouldReceive('delete')->once()->andReturn(true);
        $groupDeleted = $this->organizationGroupRepository->deleteGroup(1);
        $this->assertTrue($groupDeleted);
        $this->assertNotNull($groupDeleted);
    }

    /**
     * Test create group with no pending default groups
     *
     * @return void
     */
    public function testCreateGroupWithNoPendingDefaultGroups(): void
    {
        $this->organizationGroupMock->shouldReceive('where')->once()
            ->with('organisation_id', 3)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('where')->once()
            ->with('name', 'like', '%Group %')->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('get')->once()->andReturn(Collection::make([]));
        $this->organizationGroupMock->shouldReceive('create')->once()
            ->with(['organisation_id' => 3, 'name' => 'Group 1'])->andReturnSelf();
        $createdGroup = $this->organizationGroupRepository->createGroup(3);
        $this->assertNotNull($createdGroup);
        $this->assertInstanceOf(OrganizationGroup::class, $createdGroup);
    }

    /**
     * Test create group with pending default groups
     *
     * @return void
     */
    public function testCreateGroupWithPendingDefaultGroups(): void
    {
        $this->organizationGroupMock->shouldReceive('where')->once()
            ->with('organisation_id', 3)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('where')->once()
            ->with('name', 'like', '%Group %')->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('get')->once()
            ->andReturn(Collection::make($this->createModelsWithDefaultNames()));
        $this->organizationGroupMock->shouldReceive('create')->once()
            ->with(['organisation_id' => 3, 'name' => 'Group 2'])->andReturnSelf();
        $createdGroup = $this->organizationGroupRepository->createGroup(3);
        $this->assertNotNull($createdGroup);
        $this->assertInstanceOf(OrganizationGroup::class, $createdGroup);
    }

    /**
     * Test check group exists with given name in organization
     *
     * @return void
     */
    public function testCheckGroupExistsWithGivenNameInOrganization(): void
    {
        $this->organizationGroupMock->shouldReceive('where')->once()
            ->with(['organisation_id' => 3, 'name' => 'Group 1'])->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('exists')->once()->andReturn(true);
        $check = $this->organizationGroupRepository->checkGroupExists('Group 1', 3);
        $this->assertNotNull($check);
        $this->assertTrue($check);
    }

    /**
     * Create a sample model
     *
     * @return OrganizationGroup
     */
    private function createModel(): OrganizationGroup
    {
        $organizationGroup = new OrganizationGroup();
        $organizationGroup->id = 2;
        $organizationGroup->name = 'Group 1';

        return $organizationGroup;
    }

    /**
     * Create models with default names
     *
     * @return array
     */
    private function createModelsWithDefaultNames(): array
    {
        $results = [];
        $organizationGroup1 = new OrganizationGroup();
        $organizationGroup1->id = 1;
        $organizationGroup1->name = 'Group 1';

        array_push($results, $organizationGroup1);

        return $results;
    }

    /**
     * Test check group exists with given name in organization and groupId
     *
     * @return void
     */
    public function testCheckGroupExistsWithGivenNameInOrganizationAndGroupId(): void
    {
        $this->organizationGroupMock->shouldReceive('where')->twice()
            ->with(['organisation_id' => 3, 'name' => 'Group 1'])->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('first')->twice()->andReturn($this->createModel());
        $groupExists = $this->organizationGroupRepository->checkGroupExists('Group 1', 3, 1);
        $groupDoesNotExist = $this->organizationGroupRepository->checkGroupExists('Group 1', 3, 2);
        $this->assertTrue($groupExists);
        $this->assertFalse($groupDoesNotExist);
    }

    /**
     * Test get groups by organization
     *
     * @return void
     */
    public function testGetGroupsByOrganization(): void
    {
        $this->organizationGroupMock->shouldReceive('where')->once()
            ->with('organisation_id', 1)->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('orderBy')->once()
            ->with('created_at', 'desc')->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('with')->once()->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('get')->once()
            ->andReturn(Collection::make($this->createModelsWithDefaultNames()));
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId(1);
        $this->assertNotNull($groups);
        $this->assertNotEmpty($groups);
        $this->assertEquals(1, $groups[0]->id);
    }

    /**
     * Test it can get user's groups
     *
     * @return void
     */
    public function testOrganizationGroupsThatUserBelongsTo(): void
    {
        $this->organizationGroupMock->shouldReceive('getAttribute')
            ->with('id')->andReturn(1);
        $this->organizationGroupMock->shouldReceive('getAttribute')
            ->with('name')->andReturn('Group 1');
        $this->organizationGroupMock->shouldReceive('getAttribute')
            ->with('organisation_id')->andReturn(3);
        $this->organizationGroupMock->shouldReceive('whereHas')
            ->with('users', Mockery::on(function ($query) {
                $mockQueryBuilder = Mockery::mock(Builder::class);
                $mockQueryBuilder->shouldReceive('where')->with('user_id', 5)->andReturn($mockQueryBuilder);
                $query($mockQueryBuilder);
                return true;
            }))->andReturnSelf();
        $this->organizationGroupMock->shouldReceive('get')->andReturn(Collection::make([$this->organizationGroupMock]));
        $organizationGroups = $this->organizationGroupRepository->getUserGroups(5);
        $this->assertNotEmpty($organizationGroups);
    }
}
