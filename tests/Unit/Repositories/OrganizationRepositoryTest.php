<?php

namespace Tests\Unit\Repositories;

use App\Models\Industry;
use App\Models\Organization;
use App\Repositories\Concrete\OrganizationRepository;
use Mockery;
use Tests\TestCase;

class OrganizationRepositoryTest extends TestCase
{
    /**
     * Mocked Organization Model
     */
    protected $organizationMock;

    protected $industryMock;

    /**
     * Mocked User Repository
     *
     * @var mixed
     */
    protected $organizationRepository;

    /**
     * Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->organizationMock = Mockery::mock(Organization::class);
        $this->industryMock = Mockery::mock(Industry::class);
        $this->app->instance(Organization::class, $this->organizationMock);
        $this->app->instance(Industry::class, $this->industryMock);
        $this->organizationRepository = $this->app->make(OrganizationRepository::class);
    }

    /**
     * Test get organization by Id and returns data
     *
     * @return void
     */
    public function testGetById(): void
    {
        $this->organizationMock->shouldReceive('getAttribute')->with('id')->andReturn(1);
        $this->organizationMock->shouldReceive('find')->with(1)->andReturnSelf();
        $organization = $this->organizationRepository->getById(1);
        $this->assertInstanceOf(Organization::class, $organization);
        $this->assertNotNull($organization);
        $this->assertEquals(1, $organization->id);
    }

    /**
     * Test get organization by Id
     *
     * @return void
     */
    public function testGetByIdAndReturnsNull(): void
    {
        $this->organizationMock->shouldReceive('find')->with(1)->andReturnNull();
        $organization = $this->organizationRepository->getById(1);
        $this->assertNull($organization);
    }

    /**
     * Test get organization by Id with industry
     *
     * @return void
     */
    public function testGetByIdWithIndustry(): void
    {
        $this->organizationMock->shouldReceive('where')->with('id', 1)->andReturnSelf();
        $this->organizationMock->shouldReceive('with')->with('industry')->andReturnSelf();
        $this->organizationMock->shouldReceive('first')->andReturnSelf();
        $this->organizationMock->shouldReceive('getAttribute')->with('industry')->andReturn($this->industryMock);
        $organization = $this->organizationRepository->getByIdWithIndustry(1);
        $this->assertInstanceOf(Organization::class, $organization);
        $this->assertIsObject($organization->industry);
    }

    /**
     *  Tear down the test case
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
