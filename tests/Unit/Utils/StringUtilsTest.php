<?php

namespace Tests\Unit\Utils;

use App\Utils\StringUtils;
use Tests\TestCase;

class StringUtilsTest extends TestCase
{

    /**
     * Check if string is json
     *
     * @return void
     */
    public function testIsJsonChecksIfStringIsJson(): void
    {
        $jsonData = "[\"test\", \"test\"]";
        $stringData = "Owner";
        $isJson = StringUtils::isJson($jsonData);
        $notJson = StringUtils::isJson($stringData);
        $this->assertEquals(true, $isJson);
        $this->assertEquals(false, $notJson);
    }
}
