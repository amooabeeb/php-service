<?php

namespace Tests\Unit\Services;

use App\Services\JwtService;
use Illuminate\Support\Facades\Http;
use Lcobucci\JWT\Encoding\CannotDecodeContent;
use Lcobucci\JWT\Signer\Key;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class JwtServiceTest extends TestCase
{

    /**
     * Test function to get jwt key from cache
     *
     * @return void
     */
    public function testReturnsJwtInCacheFirstAndMakesNoOtherCall(): void
    {
        Cache::shouldReceive('get')->once()->with('jwt_key')->andReturn(new JwtKey('key', 'pass'));
        $jwtInCache = JwtService::getJwtKey('testClientId');
        $this->assertNotNull($jwtInCache);
        $this->assertEquals('key', $jwtInCache->contents());
    }

    /**
     * Test function to get jwt key from Auth service
     *
     * @return void
     */
    public function testReturnsJwtFromAuthServiceIfNotInCache(): void
    {
        Cache::shouldReceive('get')->with('jwt_key')->andReturn(null);
        Http::fake([
            '*' => Http::sequence()
                ->push([], 200)
                ->push(['key' => 'key'], 200)
        ]);
        $nullJwtKeyFromAuth = JwtService::getJwtKey('testClient');
        $this->assertNull($nullJwtKeyFromAuth);
    }
}

// phpcs:disable
class JwtKey implements Key
{

    private string $contents;
    private string $passPhrase;

    public function __construct(string $content, string $passPhrase)
    {
        $this->contents = $content;
        $this->passPhrase = $passPhrase;
    }

    public function contents(): string
    {
        return $this->contents;
    }

    public function passphrase(): string
    {
        return $this->passPhrase;
    }

    public static function base64Encoded(string $contents, string $passphrase = ''): self
    {
        $decoded = base64_decode($contents, true);

        if ($decoded === false) {
            throw CannotDecodeContent::invalidBase64String();
        }

        return new self($decoded, $passphrase);
    }
}
