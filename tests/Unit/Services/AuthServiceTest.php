<?php

namespace Tests\Unit\Services;

use App\Services\AuthService;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class AuthServiceTest extends TestCase
{

    /**
     * Test Auth Service method to get JWT key
     *
     * @return void
     */
    public function testGetJwtKeyError(): void
    {
        $clientId = 'randomclientId';
        Http::fake();
        $key = AuthService::getJWTKey($clientId);
        $this->assertNull($key);
    }

    /**
     * Test Auth Service method to get JWT key
     *
     * @return void
     */
    public function testGetJwtKeySuccess(): void
    {
        $clientId = 'randomclientId';
        Http::fake([
            '*' => Http::response(['key' => 'test_jwt_key'])
        ]);
        $key = AuthService::getJWTKey($clientId);
        $this->assertNotNull($key);
        $this->assertEquals('test_jwt_key', $key);
    }
}
