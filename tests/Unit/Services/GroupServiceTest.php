<?php

namespace Tests\Unit\Services;

use App\Services\GroupService;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class GroupServiceTest extends TestCase
{
    /**
     * Test group has active assessment
     *
     * @return void
     */
    public function testHasActiveAssessment(): void
    {
        Http::fake([
            '*' => Http::sequence()
                ->push(['has_assessment' => false], 200)
                ->push([], 200)
        ]);
        $hasAssessment = GroupService::hasActiveAssessment(1, 'testClientId', 'testToken');
        $emptyResponse = GroupService::hasActiveAssessment(1, 'testClientId', 'testToken');
        $this->assertFalse($hasAssessment);
        $this->assertTrue($emptyResponse);
    }
}
