<?php

namespace Tests\Unit\Controllers;

use App\Http\Controllers\OrganizationController;
use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use App\Repositories\Contracts\OrganizationRepositoryInterface;
use App\Repositories\Contracts\RecruiterDetailRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery;
use Tests\TestCase;

class OrganizationControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Mocked Organization Repository
     */
    protected $organizationRepositoryMock;

    /**
     * Mocked Recruiter Detail Repository
     */
    protected $recruiterDetailRepositoryMock;

    /**
     * Mocked User Repository
     */
    protected $userRepositoryMock;

    /**
     * Mocked User Model
     */
    protected $userMock;

    /**
     * Mocked Organization Model
     */
    protected $organizationMock;

    /**
     * Mocked Recruiter Detail Model
     */
    protected $recruiterDetailMock;


    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->userMock = Mockery::mock(User::class);
        $this->organizationMock = Mockery::mock(Organization::class);
        $this->recruiterDetailMock = Mockery::mock(RecruiterUserDetail::class);

        $this->userRepositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $this->organizationRepositoryMock = Mockery::mock(OrganizationRepositoryInterface::class);
        $this->recruiterDetailRepositoryMock = Mockery::mock(RecruiterDetailRepositoryInterface::class);
    }

    /**
     * Assert response object is a 400
     *
     * @param JsonResponse $response
     *
     * @return void
     */
    private function assert400ForInvalidOrganization(JsonResponse $response): void
    {
        $this->assertEquals(400, $response->status());
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Assert response object is a 403
     *
     * @param JsonResponse $response
     *
     * @return void
     */
    private function assert403ForUnAuthorizedUser(JsonResponse $response): void
    {
        $this->assertEquals(403, $response->status());
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test for Controller GetOrganizationDetails
     *
     * @return void
     */
    public function testUserCanGetOrganizationDetails(): void
    {
        $recruiters = Collection::make([$this->recruiterDetailMock]);
        $this->organizationRepositoryMock
            ->shouldReceive('getByIdWithIndustry')
            ->once()->with(5)
            ->andReturn(new Organization());
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('getRecruitersForOrganization')
            ->once()->andReturn($recruiters);
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('canAccessOrganization')
            ->with($recruiters, 3)
            ->andReturn(true);
        $request = Request::create('/organizations/5');
        $request->merge(['auth' => ['userId' => 3]]);
        $controller = new OrganizationController(
            $this->organizationRepositoryMock,
            $this->userRepositoryMock,
            $this->recruiterDetailRepositoryMock
        );
        $response = $controller->getOrganizationDetails($request, 5);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->status());
    }

    /**
     * Test for Controller GetOrganizationDetails
     *
     * @return void
     */
    public function testUserCannotAccessOrganizationDetails(): void
    {
        $recruiters = Collection::make([$this->recruiterDetailMock]);
        $this->organizationRepositoryMock
            ->shouldReceive('getByIdWithIndustry')
            ->once()->with(5)
            ->andReturn(new Organization());
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('getRecruitersForOrganization')
            ->once()->with(5)
            ->andReturn($recruiters);
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('canAccessOrganization')
            ->with($recruiters, 4)
            ->andReturn(false);
        $request = Request::create('/organizations/5');
        $request->merge(['auth' => ['userId' => 4]]);
        $controller = new OrganizationController(
            $this->organizationRepositoryMock,
            $this->userRepositoryMock,
            $this->recruiterDetailRepositoryMock
        );
        $response = $controller->getOrganizationDetails($request, 5);
        $this->assert403ForUnAuthorizedUser($response);
    }

    /**
     * Test for Controller GetOrganizationDetails
     *
     * @return void
     */
    public function testOrganizationNotAvailable(): void
    {
        $this->organizationRepositoryMock
            ->shouldReceive('getByIdWithIndustry')
            ->once()->with(5)
            ->andReturn(null);
        $request = Request::create('/organizations/5');
        $request->merge(['auth' => ['userId' => 4]]);
        $controller = new OrganizationController(
            $this->organizationRepositoryMock,
            $this->userRepositoryMock,
            $this->recruiterDetailRepositoryMock
        );
        $response = $controller->getOrganizationDetails($request, 5);
        $this->assert400ForInvalidOrganization($response);
    }

    /**
     * Test for Controller GetOrganizationUsers
     *
     * @return void
     */
    public function testUsersCanGetOrganizationUsers(): void
    {
        $recruiters = Collection::make([$this->recruiterDetailMock]);
        $organizationUsers = Collection::make([new User()]);
        $this->organizationRepositoryMock
            ->shouldReceive('getById')
            ->once()->with(5)
            ->andReturn($this->organizationMock);
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('getRecruitersForOrganization')
            ->once()->andReturn($recruiters);
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('canAccessOrganization')
            ->with($recruiters, 3)
            ->andReturn(true);
        $this->userRepositoryMock
            ->shouldReceive('getOrganizationUsers')
            ->once()->with(5)
            ->andReturn($organizationUsers);
        $request = Request::create('/organizations/5/users');
        $request->merge(['auth' => ['userId' => 3]]);
        $controller = new OrganizationController(
            $this->organizationRepositoryMock,
            $this->userRepositoryMock,
            $this->recruiterDetailRepositoryMock
        );
        $response = $controller->getOrganizationUsers($request, 5);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
    }

    /**
     * Test for Controller GetOrganizationUsers
     *
     * @return void
     */
    public function testItReturns403ForUnauthorizedUsersToGetOrganizationUsers(): void
    {
        $recruiters = Collection::make([$this->recruiterDetailMock]);
        $this->organizationRepositoryMock
            ->shouldReceive('getById')
            ->once()->with(5)
            ->andReturn($this->organizationMock);
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('getRecruitersForOrganization')
            ->once()->andReturn($recruiters);
        $this->recruiterDetailRepositoryMock
            ->shouldReceive('canAccessOrganization')
            ->with($recruiters, 3)
            ->andReturn(false);
        $request = Request::create('/organizations/5/users');
        $request->merge(['auth' => ['userId' => 3]]);
        $controller = new OrganizationController(
            $this->organizationRepositoryMock,
            $this->userRepositoryMock,
            $this->recruiterDetailRepositoryMock
        );
        $response = $controller->getOrganizationUsers($request, 5);
        $this->assert403ForUnAuthorizedUser($response);
    }

    /**
     * Test for Controller GetOrganizationUsers
     *
     * @return void
     */
    public function testItReturns400ToGetInvalidOrganizationUsers(): void
    {
        $this->organizationRepositoryMock
            ->shouldReceive('getById')
            ->once()->with(5)
            ->andReturn(null);
        $request = Request::create('/organizations/5/users');
        $request->merge(['auth' => ['userId' => 4]]);
        $controller = new OrganizationController(
            $this->organizationRepositoryMock,
            $this->userRepositoryMock,
            $this->recruiterDetailRepositoryMock
        );
        $response = $controller->getOrganizationUsers($request, 5);
        $this->assert400ForInvalidOrganization($response);
    }

    /**
     *  Tears down the testcase
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
