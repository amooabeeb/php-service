<?php

namespace Tests\Unit\Controllers;

use App\Http\Controllers\UserController;
use App\Http\Requests\UpdateUserRequest;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use App\Models\UserGoogle;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use App\Repositories\Contracts\SocialAuthRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Mocked User Repository
     */
    protected $userRepositoryMock;

    /**
     * Mocked Social Auth Repository
     */
    protected $socialAuthRepositoryMock;

    /**
     * Mocked Recruiter Detail Model
     */
    protected $recruiterDetailMock;

    /**
     * Mocked Organization Group Repository
     */
    protected $organizationGroupRepositoryMock;

    /**
     * Instance of User Controller
     */
    protected UserController $userController;

    /**
     * Mocked User Model
     */
    protected $userMock;

    /**
     * Mocked Google User Model
     */
    protected $userGoogleMock;

    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->userMock = Mockery::mock(User::class);
        $this->userGoogleMock = Mockery::mock(UserGoogle::class);
        $this->recruiterDetailMock = Mockery::mock(RecruiterUserDetail::class);
        $this->userRepositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $this->socialAuthRepositoryMock = Mockery::mock(SocialAuthRepositoryInterface::class);
        $this->organizationGroupRepositoryMock = Mockery::mock(OrganizationGroupRepositoryInterface::class);

        $this->app->instance(User::class, $this->userMock);
        $this->app->instance(UserGoogle::class, $this->userGoogleMock);
        $this->app->instance(SocialAuthRepositoryInterface::class, $this->socialAuthRepositoryMock);
        $this->app->instance(UserRepositoryInterface::class, $this->userRepositoryMock);
        $this->app->instance(OrganizationGroupRepositoryInterface::class, $this->organizationGroupRepositoryMock);
        $this->userController = app(UserController::class);
    }

    /**
     * Assert response object is a 403
     *
     * @param JsonResponse $response
     *
     * @return void
     */
    private function assert403ForUnAuthorizedUser(JsonResponse $response): void
    {
        $this->assertEquals(403, $response->status());
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test for Controller GetUserDetails
     *
     * @return void
     */
    public function testGetUserDetails(): void
    {
        $this->userRepositoryMock->shouldReceive('getById')->once()->with(5)->andReturn(new User());
        $this->userRepositoryMock->shouldReceive('canAccessUserDetails')->once()->with(5, 5)->andReturn(true);
        $request = Request::create('/users/5');
        $request->merge(['auth' => ['userId' => 5]]);
        $response = $this->userController->getUserDetails($request, 5);
        $this->assertInstanceOf(JsonResponse::class, $response);

        $this->userRepositoryMock->shouldReceive('canAccessUserDetails')->once()->with(5, 5)->andReturn(false);
        $response = $this->userController->getUserDetails($request, 5);
        $this->assert403ForUnAuthorizedUser($response);
    }

    /**
     * Test for Controller GetAllTimezones
     *
     * @return void
     */
    public function testGetAllTimezones(): void
    {
        $timezone = [
            'code' => 'Africa/Abidjan',
            'offset' => '+00:00'
        ];
        $this->userRepositoryMock->shouldReceive('getAllTimezones')->once()->andReturn([$timezone]);
        $response = $this->userController->getAllTimezones();
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
    }

    /**
     * Test for Controller updateProfile
     *
     * @return void
     */
    public function testCanUpdateUserDetails(): void
    {
        $updateData = ['user_timezone' => 'Africa/Abidjan'];
        $this->userRepositoryMock
            ->shouldReceive('updateUserDetails')
            ->with(5, $updateData)
            ->andReturn(new User());
        $this->userRepositoryMock
            ->shouldReceive('canAccessUserDetails')
            ->with(5, 5)
            ->once()->andReturn(true);
        $request = Mockery::mock(UpdateUserRequest::create('/users/5'));
        $request->merge(['auth' => ['userId' => 5]]);
        $request->shouldReceive('validated')->andReturn($updateData);
        $response = $this->userController->updateProfile($request, 5);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);

        $this->userRepositoryMock
            ->shouldReceive('canAccessUserDetails')
            ->with(5, 5)
            ->once()->andReturn(false);
        $response = $this->userController->updateProfile($request, 5);
        $this->assert403ForUnAuthorizedUser($response);
    }

    /**
     * Test it formats user permissions appropriately
     *
     * @return void
     */
    public function testItFormatsUserPermissionsCorrectly(): void
    {
        $jsonData = "[\"test\", \"test\"]";
        $stringData = "Owner";
        $jsonPermissions = $this->userController->formatUserPermissions($jsonData);
        $stringPermissions = $this->userController->formatUserPermissions($stringData);
        $this->assertIsArray($jsonPermissions);
        $this->assertEquals(['test', 'test'], $jsonPermissions);
        $this->assertIsArray($stringPermissions);
        $this->assertEquals(['Owner'], $stringPermissions);
    }

    /**
     * Test for that user can get permissions
     *
     * @return void
     */
    public function testCanGetPermissions(): void
    {
        $this->userMock->shouldReceive('getAttribute')->with('user_permissions')->andReturn('Owner');
        $this->userMock->shouldReceive('getAttribute')->with('user_type')->andReturn('recruiter');
        $this->userRepositoryMock->shouldReceive('getByIdWithProfile')->with(5)->andReturn($this->userMock);
        $request = Request::create('/users/5/permissions');
        $response = $this->userController->getUserPermissions($request, 5);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('user_permissions', $decodedResponse['data']);
        $this->assertEquals(['Owner'], $decodedResponse['data']['user_permissions']);
    }

    /**
     * Test user's social auth profile error
     *
     * @return void
     */
    public function testGetUserSocialAuthError(): void
    {
        $this->userGoogleMock->shouldReceive('getAttribute')->with('user_id')->andReturn(5);
        $this->socialAuthRepositoryMock
            ->shouldReceive('getSocialAuthBySocialiteId')
            ->with('123wde', 'google')
            ->once()
            ->andReturn(null);
        $request = Request::create('/users/google/123wde');
        $response = $this->userController->getSocialAuthProfile($request, 'google', '123wde');
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertEquals(422, $response->status());
    }

    /**
     * Test user's social auth profile success
     *
     * @return void
     */
    public function testGetUserSocialAuthSuccess(): void
    {
        $this->userGoogleMock->shouldReceive('getAttribute')->with('user_id')->andReturn(5);
        $this->socialAuthRepositoryMock
            ->shouldReceive('getSocialAuthBySocialiteId')
            ->with('123wde', 'google')
            ->once()
            ->andReturn(new UserGoogle());
        $request = Request::create('/users/google/123wde');
        $response = $this->userController->getSocialAuthProfile($request, 'google', '123wde');
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertEquals(200, $response->status());
    }

    /**
     * Test get all groups for a user
     *
     * @return void
     */
    public function testGetAllGroupsForAUserInAnOrganization(): void
    {
        $request = Request::create('/users/5/groups');

        $recruiterDetail = new RecruiterUserDetail();
        $recruiterDetail->organization_id = 8;

        $this->userMock->shouldReceive('getAttribute')->with('recruiterDetail')->andReturn($recruiterDetail);
        $this->userRepositoryMock->shouldReceive('getByIdWithProfile')->once()->with(5)->andReturn($this->userMock);
        $this->organizationGroupRepositoryMock->shouldReceive('getUserGroups')
            ->once()->with(5)->andReturn(Collection::make([]));

        $response = $this->userController->getUserGroups($request, 5);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('organisation_id', $decodedResponse['data']);
        $this->assertArrayHasKey('groups', $decodedResponse['data']);
        $this->assertEmpty($decodedResponse['data']['groups']);
    }

    /**
     *  Tears down the testcase
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
