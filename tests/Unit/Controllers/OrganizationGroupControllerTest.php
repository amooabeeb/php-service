<?php

namespace Tests\Unit\Controllers;

use App\Http\Controllers\OrganizationGroupController;
use App\Http\Requests\SyncUserToGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Models\OrganizationGroup;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use App\Repositories\Concrete\OrganizationGroupRepository;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Mockery\MockInterface;
use Tests\TestCase;

class OrganizationGroupControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Mocked User Model
     */
    protected $userMock;

    /**
     * Mocked OrganizationGroupTest Model
     */
    protected $organizationGroupMock;

    /**
     * Mocked RecruiterDetail Model
     */
    protected $recruiterDetailMock;

    /**
     * Mocked OrganizationGroupTest Repository
     */
    protected $organizationGroupRepositoryMock;

    /**
     * Instance of OrganizationGroupController
     */
    protected OrganizationGroupController $organizationGroupController;

    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->recruiterDetailMock = $this->mock(RecruiterUserDetail::class, function (MockInterface $mock) {
            $mock->shouldReceive('getAttribute')->with('organisation_id')->andReturn(1);
        });
        $this->userMock = $this->mock(User::class, function (MockInterface $mock) {
            $mock->shouldReceive('getAttribute')->with('recruiterDetail')->andReturn($this->recruiterDetailMock);
        });
        $this->organizationGroupMock = $this->mock(OrganizationGroup::class);
        $this->organizationGroupRepositoryMock = $this->mock(OrganizationGroupRepository::class);
        $this->organizationGroupController = app(OrganizationGroupController::class);
    }

    /**
     * Get instance of request from a recruiter admin
     *
     * @param string $url
     *
     * @return Request
     */
    private function createRequestFromRecruiterAdmin(string $url): Request
    {
        $request = Request::create($url);
        $request->merge(['auth' => ['userId' => 5]]);
        $request->merge(['user' => $this->userMock]);
        return $request;
    }

    /**
     * Get instance of SyncUserToGroupRequest
     *
     * @return SyncUserToGroupRequest
     */
    private function createSyncUserRequest(): SyncUserToGroupRequest
    {
        $request = SyncUserToGroupRequest::create('/groups/5/users');
        $request->merge(['auth' => ['userId' => 5]]);
        $request->merge(['user' => $this->userMock]);
        $request->merge(['user_id' => 15]);
        return $request;
    }

    /**
     * Create OrganizationGroupTest model
     *
     * @return OrganizationGroup
     */
    private function createModel(): OrganizationGroup
    {
        $organizationGroup = new OrganizationGroup();
        $organizationGroup->id = 5;

        return $organizationGroup;
    }

    /**
     * Assert fetch group success
     *
     * @param JsonResponse $response
     *
     * @return void
     */
    private function assertFetchGroupsSuccess(JsonResponse $response): void
    {
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->status());
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['data']);
    }

    /**
     * Assert client error
     *
     * @param JsonResponse $response
     * @param int          $statusCode
     *
     * @return void
     */
    private function assertClientError(JsonResponse $response, int $statusCode = 422): void
    {
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($statusCode, $response->status());
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayNotHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     * Test get all groups for an organization
     *
     * @return void
     */
    public function testGetAllGroupsForAnOrganization(): void
    {
        $request = $this->createRequestFromRecruiterAdmin('/groups');
        $this->organizationGroupRepositoryMock->shouldReceive('getGroupsByOrganizationId')
            ->once()->with(1)->andReturn(Collection::make([$this->createModel()]));
        $response = $this->organizationGroupController->getAllGroupsForAnOrganization($request);
        $this->assertFetchGroupsSuccess($response);
    }

    /**
     * Test create group for an organization
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testCreateGroup(): void
    {
        $request = $this->createRequestFromRecruiterAdmin('/groups');
        $this->organizationGroupRepositoryMock->shouldReceive('createGroup')
            ->once()->with(1)->andReturn($this->createModel());
        $this->organizationGroupRepositoryMock->shouldReceive('getGroupsByOrganizationId')
            ->once()->with(1)->andReturn(Collection::make([$this->createModel()]));
        $response = $this->organizationGroupController->createGroup($request);
        $this->assertFetchGroupsSuccess($response);
    }

    /**
     * Test add user to group for an organization successfully
     *
     * @return void
     */
    public function testAddUserToGroupSuccess(): void
    {
        $request = $this->createSyncUserRequest();
        $this->organizationGroupRepositoryMock->shouldReceive('addUserToGroup')
            ->once()->with(5, 15)->andReturn(true);
        $this->organizationGroupRepositoryMock->shouldReceive('getGroupsByOrganizationId')
            ->once()->with(1)->andReturn(Collection::make([$this->createModel()]));
        $response = $this->organizationGroupController->addUserToGroup($request, 5);
        $this->assertFetchGroupsSuccess($response);
    }

    /**
     * Test add user to group for an organization error
     *
     * @return void
     */
    public function testAddUserToGroupError(): void
    {
        $request = $this->createSyncUserRequest();
        $this->organizationGroupRepositoryMock->shouldReceive('addUserToGroup')
            ->once()->with(5, 15)->andReturn(false);
        $response = $this->organizationGroupController->addUserToGroup($request, 5);
        $this->assertClientError($response);
    }

    /**
     * Test detach user to group for an organization successfully
     *
     * @return void
     */
    public function testDetachUserFromGroupSuccess(): void
    {
        $request = $this->createRequestFromRecruiterAdmin('groups/5/users/8');
        $this->organizationGroupRepositoryMock->shouldReceive('deleteUserFromGroup')
            ->once()->with(5, 8)->andReturn(true);
        $this->organizationGroupRepositoryMock->shouldReceive('getGroupsByOrganizationId')
            ->once()->with(1)->andReturn(Collection::make([$this->createModel()]));
        $response = $this->organizationGroupController->detachUserFromGroup($request, 5, 8);
        $this->assertFetchGroupsSuccess($response);
    }

    /**
     * Test detach user to group for an organization error
     *
     * @return void
     */
    public function testDetachUserFromGroupError(): void
    {
        $request = $this->createRequestFromRecruiterAdmin('groups/5/users/8');
        $this->organizationGroupRepositoryMock->shouldReceive('deleteUserFromGroup')
            ->once()->with(5, 15)->andReturn(false);
        $response = $this->organizationGroupController->detachUserFromGroup($request, 5, 15);
        $this->assertClientError($response);
    }

    /**
     * Test cannot delete group with assessment
     *
     * @return void
     */
    public function testCannotDeleteGroupWithAssessment(): void
    {
        $request = $this->createRequestFromRecruiterAdmin('groups/5');
        $request = \Mockery::mock($request);
        $request->shouldReceive('bearerToken')->andReturn('testToken');
        $request->shouldReceive('input')->with('client_id')->andReturn('testClientId');
        Http::fake([
            '*' => Http::response(['has_assessment' => true])
        ]);
        $response = $this->organizationGroupController->deleteGroup($request, 5);
        $this->assertClientError($response, 400);
    }

    /**
     * Test delete group without assessment
     *
     * @return void
     */
    public function testDeleteGroupWithoutAssessment(): void
    {
        $request = $this->createRequestFromRecruiterAdmin('groups/5');
        $request = \Mockery::mock($request);
        $request->shouldReceive('bearerToken')->andReturn('testToken');
        $request->shouldReceive('input')->with('client_id')->andReturn('testClientId');
        Http::fake([
            '*' => Http::response(['has_assessment' => false])
        ]);
        $this->organizationGroupRepositoryMock->shouldReceive('deleteGroup')
            ->once()->with(5)->andReturn(true);
        $this->organizationGroupRepositoryMock->shouldReceive('getGroupsByOrganizationId')
            ->once()->with(1)->andReturn(Collection::make([$this->createModel()]));
        $response = $this->organizationGroupController->deleteGroup($request, 5);
        $this->assertFetchGroupsSuccess($response);
    }

    /**
     * Test cannot update group with duplicate names
     *
     * @return void
     */
    public function testCannotUpdateGroupWithDuplicateNames(): void
    {
        $updateData = ['name' => 'Group 1'];
        $request = \Mockery::mock(UpdateGroupRequest::create('/groups/5'));
        $request->shouldReceive('validated')->andReturn($updateData);
        $request->merge(['auth' => ['userId' => 5]]);
        $request->merge(['user' => $this->userMock]);
        $this->organizationGroupRepositoryMock->shouldReceive('checkGroupExists')
            ->once()->with('Group 1', 1, 5)->andReturn(true);
        $response = $this->organizationGroupController->updateGroup($request, 5);
        $this->assertClientError($response);
    }

    /**
     * Test can update group
     *
     * @return void
     */
    public function testUpdateGroupSuccess(): void
    {
        $updateData = ['name' => 'Group 1'];
        $request = \Mockery::mock(UpdateGroupRequest::create('/groups/5'));
        $request->shouldReceive('validated')->andReturn($updateData);
        $request->merge(['auth' => ['userId' => 5]]);
        $request->merge(['user' => $this->userMock]);
        $this->organizationGroupRepositoryMock->shouldReceive('checkGroupExists')
            ->once()->with('Group 1', 1, 5)->andReturn(false);
        $this->organizationGroupRepositoryMock->shouldReceive('updateGroupWithUsers')
            ->once()->with(5, $updateData)->andReturn($this->createModel());
        $this->organizationGroupRepositoryMock->shouldReceive('getGroupsByOrganizationId')
            ->once()->with(1)->andReturn(Collection::make([$this->createModel()]));
        $response = $this->organizationGroupController->updateGroup($request, 5);
        $this->assertFetchGroupsSuccess($response);
    }
}
