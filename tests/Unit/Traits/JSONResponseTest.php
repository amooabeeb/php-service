<?php

namespace Tests\Unit\Traits;

use App\Traits\JSONResponse;
use Mockery\Exception;
use Tests\TestCase;

class JSONResponseTest extends TestCase
{

    private object $jsonResponseMock;

    /**
     *  Sets up the testcase
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->jsonResponseMock = $this->getObjectForTrait(JSONResponse::class);
    }


    /**
     * Test send success response
     *
     * @return void
     */
    public function testSendSuccessResponse(): void
    {
        $successResponse = $this->jsonResponseMock->sendSuccessResponse(['test' => 'ok']);
        $successResponseWithStatus = $this->jsonResponseMock->sendSuccessResponse(['test' => 'ok'], 204);
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $successResponse);
        $this->assertEquals(200, $successResponse->status());
        $this->assertEquals(204, $successResponseWithStatus->status());
        $decodedResponse = json_decode($successResponse->getContent(), true);
        $this->assertArrayHasKey('data', $decodedResponse);
        $this->assertArrayHasKey('test', $decodedResponse['data']);
    }

    /**
     * Test send client response
     *
     * @return void
     */
    public function testSendClientErrorResponse(): void
    {
        $errorResponse = $this->jsonResponseMock->sendClientErrorResponse(
            ['detail' => 'Detail', 'description' => 'description']
        );
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $errorResponse);
        $this->assertEquals(422, $errorResponse->status());
        $decodedResponse = json_decode($errorResponse->getContent(), true);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertIsArray($decodedResponse['errors']);
    }

    /**
     * Test send server response
     *
     * @return void
     */
    public function testSendFatalErrorResponse(): void
    {
        $errorResponse = $this->jsonResponseMock->sendServerErrorResponse(new Exception());
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $errorResponse);
        $this->assertEquals(500, $errorResponse->status());
        $decodedResponse = json_decode($errorResponse->getContent(), true);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertIsArray($decodedResponse['errors']);
    }
}
