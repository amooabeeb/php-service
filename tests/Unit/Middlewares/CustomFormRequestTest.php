<?php

namespace Tests\Unit\Middlewares;

use App\Http\Requests\CustomFormRequest;
use Illuminate\Support\MessageBag;
use Tests\TestCase;

class CustomFormRequestTest extends TestCase
{

    /**
     * Test that validation errors are properly formatted
     *
     * @return void
     */
    public function testCanFormatValidationErrorMessages(): void
    {
        $validationErrors = [
            'first_name' => [
                'First name is required',
                'First name must be more than 8 characters'
            ],
            'email' => [
                'Email is required',
                'Email must be unique'
            ]
        ];
        $customRequest = new TestFormRequest();
        $errors  = $customRequest->getErrorMessages($validationErrors);
        $this->assertIsArray($errors);
        $this->assertIsArray($errors[0]);
        $this->assertArrayHasKey('detail', $errors[0]);
    }
}

// phpcs:disable
class TestFormRequest extends CustomFormRequest {

}
