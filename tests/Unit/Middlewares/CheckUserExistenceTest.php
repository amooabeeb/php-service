<?php

namespace Tests\Unit\Middlewares;

use App\Http\Middleware\CheckUserExistence;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery;
use Tests\TestCase;

class CheckUserExistenceTest extends TestCase
{

    /**
     * Mocked User Repository
     */
    protected $userRepositoryMock;

    /**
     * Mocked request class
     */
    protected $requestMock;

    /**
     * Instance of CheckUserExistence middleware
     */
    protected CheckUserExistence $checkUserExistenceMiddleware;

    /**
     * Set up the test case.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->userRepositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $this->requestMock = Mockery::mock(Request::class);
        $this->checkUserExistenceMiddleware = app(CheckUserExistence::class);
    }

    /**
     * Test that middleware returns appropriate error
     *
     * @return void
     */
    public function testItReturnsErrorForInvalidUser(): void
    {
        $this->requestMock->shouldReceive('route')->once()->with('id')->andReturn(5);
        $this->userRepositoryMock
            ->shouldReceive('getById')
            ->with(5)
            ->andReturn(null);
        $middleware = new CheckUserExistence($this->userRepositoryMock);
        $response = $middleware->handle($this->requestMock, function () {
            return true;
        });
        $this->assertEquals(400, $response->status());
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertObjectHasAttribute('data', $response);
        $decodedResponse = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('errors', $decodedResponse);
        $this->assertNotEmpty($decodedResponse['errors']);
    }

    /**
     *  Tears down the testcase
     *
     * @return void
     */
    public function tearDown(): void
    {
        Mockery::close();
    }
}
