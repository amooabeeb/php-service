<?php

namespace Tests\Unit\Models;

use App\Models\CuratorUserDetail;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Mockery;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * Test relationship with organization which user owns
     *
     * @return void
     */
    public function testRelationshipWithOrganization(): void
    {
        $user = new User();
        $organizationRelationship = $user->organization();
        $this->assertInstanceOf(HasOne::class, $organizationRelationship);
    }

    /**
     * Test relationship with curator detail
     *
     * @return void
     */
    public function testRelationshipWithCuratorDetail(): void
    {
        $user = new User();
        $curatorDetailRelationship = $user->curatorDetail();
        $this->assertInstanceOf(HasOne::class, $curatorDetailRelationship);
    }

    /**
     * Test relationship with recruiter detail
     *
     * @return void
     */
    public function testRelationshipWithRecruiterDetail(): void
    {
        $user = new User();
        $recruiterDetailRelationship = $user->recruiterDetail();
        $this->assertInstanceOf(HasOne::class, $recruiterDetailRelationship);
    }

    /**
     * Test returns null permission for unknown user type
     *
     * @return void
     */
    public function testItReturnsNullPermissionForUnknownUserType(): void
    {
        $user = new User();
        $permission = $user->getPermission();

        $this->assertNull($permission);

        $user->user_type = 'curator';
        $curatorDetail = new CuratorUserDetail();
        $curatorDetail->roles_csv = 'role1|role2';
        $user->setRelation('curatorDetail', $curatorDetail);
        $permission = $user->getPermission();
        $this->assertNotNull($permission);
        $this->assertEquals('role1|role2', $permission);
    }

    /**
     * Test returns correct permission for curator user
     *
     * @return void
     */
    public function testItReturnsCorrectPermissionsForCuratorUser(): void
    {
        $user = new User();
        $user->user_type = 'curator';
        $curatorDetail = new CuratorUserDetail();
        $curatorDetail->roles_csv = 'role1|role2';
        $user->setRelation('curatorDetail', $curatorDetail);
        $permission = $user->getPermission();
        $this->assertNotNull($permission);
        $this->assertEquals('role1|role2', $permission);
    }

    /**
     * Test returns correct permission for recruiter user
     *
     * @return void
     */
    public function testItReturnsCorrectPermissionsForRecruiterUser(): void
    {
        $user = new User();
        $user->user_type = 'recruiter';
        $nullPermission = $user->getPermission();

        $recruiterDetail = new RecruiterUserDetail();
        $recruiterDetail->permissions_level = 'Owner';
        $user->setRelation('recruiterDetail', $recruiterDetail);
        $permission = $user->getPermission();

        $this->assertNotNull($permission);
        $this->assertEquals('Owner', $permission);
        $this->assertNull($nullPermission);
    }

    /**
     * Test scoped query for user profile
     *
     * @return void
     */
    public function testProfileDetailScope(): void
    {
        $user = new User();
        $mockQueryBuilder = Mockery::mock(Builder::class);
        $scopedQuery = $user->scopeWithProfileDetail($mockQueryBuilder);
        $this->assertNull($scopedQuery);


        $user->user_type = 'curator';
        $mockQueryBuilder->shouldReceive('with')->with('curatorDetail')->once()->andReturnSelf();
        $scopedQuery = $user->scopeWithProfileDetail($mockQueryBuilder);
        $this->assertInstanceOf(Builder::class, $scopedQuery);

        $user->user_type = 'recruiter';
        $mockQueryBuilder->shouldReceive('with')->with('recruiterDetail')->once()->andReturnSelf();
        $scopedQuery = $user->scopeWithProfileDetail($mockQueryBuilder);
        $this->assertInstanceOf(Builder::class, $scopedQuery);
    }
}
