<?php

namespace Tests\Unit\Models;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class OrganizationTest extends TestCase
{
    /**
     * Test relationship with industry
     *
     * @return void
     */
    public function testRelationshipWithIndustry(): void
    {
        $organization = new Organization();
        $industryRelationship = $organization->industry();
        $this->assertInstanceOf(BelongsTo::class, $industryRelationship);
    }

    /**
     * Test relationship with recruiter users
     *
     * @return void
     */
    public function testRelationshipWithRecruiterUsers(): void
    {
        $organization = new Organization();
        $recruitersRelationship = $organization->recruiters();
        $this->assertInstanceOf(HasMany::class, $recruitersRelationship);
    }

    /**
     * Test relationship with recruiter owner
     *
     * @return void
     */
    public function testRelationshipWithRecruiterOwner(): void
    {
        $organization = new Organization();
        $ownerRelationship = $organization->owner();
        $this->assertInstanceOf(BelongsTo::class, $ownerRelationship);
    }
}
