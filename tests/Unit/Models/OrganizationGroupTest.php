<?php

namespace Tests\Unit\Models;

use App\Models\OrganizationGroup;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tests\TestCase;

class OrganizationGroupTest extends TestCase
{
    /**
     * Test relationship with industry
     *
     * @return void
     */
    public function testRelationshipWithOrganization(): void
    {
        $organizationGroup = new OrganizationGroup();
        $organizationRelationship = $organizationGroup->organization();
        $this->assertInstanceOf(BelongsTo::class, $organizationRelationship);
    }

    /**
     * Test relationship with recruiter users
     *
     * @return void
     */
    public function testRelationshipWithRecruiterUsers(): void
    {
        $organization = new OrganizationGroup();
        $recruitersRelationship = $organization->users();
        $this->assertInstanceOf(BelongsToMany::class, $recruitersRelationship);
    }
}
