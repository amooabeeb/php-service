<?php

namespace Tests\Unit\Models;

use App\Models\RecruiterUserDetail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Tests\TestCase;

class RecruiterUserDetailTest extends TestCase
{
    /**
     * Test relationship with user
     *
     * @return void
     */
    public function testRelationshipWithUser(): void
    {
        $curatorUserDetail = new RecruiterUserDetail();
        $userRelationship = $curatorUserDetail->user();
        $this->assertInstanceOf(BelongsTo::class, $userRelationship);
    }

    /**
     * Test relationship with organization to which user belongs
     *
     * @return void
     */
    public function testRelationshipWithOrganization(): void
    {
        $curatorUserDetail = new RecruiterUserDetail();
        $userRelationship = $curatorUserDetail->organization();
        $this->assertInstanceOf(BelongsTo::class, $userRelationship);
    }
}
