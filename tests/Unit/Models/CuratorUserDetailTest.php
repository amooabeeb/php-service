<?php

namespace Tests\Unit\Models;

use App\Models\CuratorUserDetail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Tests\TestCase;

class CuratorUserDetailTest extends TestCase
{
    /**
     * Test relationship with user
     *
     * @return void
     */
    public function testRelationshipWithUser(): void
    {
        $curatorUserDetail = new CuratorUserDetail();
        $userRelationship = $curatorUserDetail->user();
        $this->assertInstanceOf(BelongsTo::class, $userRelationship);
    }
}
