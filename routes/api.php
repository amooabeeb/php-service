<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/healthCheck', function (Request $request) {
    return ['status' => 'ok'];
});

// Route to test errors on cloudwatch. To be removed later
Route::post('/errors/create', function (Request $request) {
    throw new Exception("This is an intentional error on the Profile service", 1);
});

Route::group(['middleware' => ['jwt-auth']], function () {

    Route::get('/organizations/{id}', 'OrganizationController@getOrganizationDetails');
    Route::get('/organizations/{id}/users', 'OrganizationController@getOrganizationUsers');

    Route::group(['prefix' => 'groups', 'middleware' => ['group-access']], function () {
        Route::get('/', 'OrganizationGroupController@getAllGroupsForAnOrganization');
        Route::post('/', 'OrganizationGroupController@createGroup');
        Route::patch('/{id}', 'OrganizationGroupController@updateGroup');
        Route::delete('/{id}', 'OrganizationGroupController@deleteGroup');
        Route::post('/{id}/users', 'OrganizationGroupController@addUserToGroup');
        Route::delete('/{id}/users/{userId}', 'OrganizationGroupController@detachUserFromGroup');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/{id}', 'UserController@getUserDetails')->middleware('user-exists');
        Route::patch('/{id}', 'UserController@updateProfile')->middleware('user-exists');
        Route::get('/{id}/groups', 'UserController@getUserGroups')->middleware('user-exists');
    });

    Route::get('/timezones', 'UserController@getAllTimezones');
});

Route::get('/users/{id}/permissions','UserController@getUserPermissions')->middleware('user-exists');
Route::get('/users/{driver}/{socialiteId}', 'UserController@getSocialAuthProfile');
