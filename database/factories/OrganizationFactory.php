<?php

namespace Database\Factories;

use App\Models\Industry;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'organisation' => ucfirst($this->faker->company),
            'domain' => $this->faker->url,
            'industry_id' => Industry::pluck('id')->random(),
            'user_id' => User::pluck('id')->random(),
            'website_url' => $this->faker->url,
            'subdomain' => $this->faker->url,
            'size_employees' => '101-500',
            'is_recruitment_company' => $this->faker->numberBetween(0, 1),
            'has_paid' => $this->faker->boolean
        ];
    }
}
