<?php

namespace Database\Factories;

use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecruiterUserDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RecruiterUserDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::pluck('id')->random(),
            'organisation_id' => Organization::pluck('id')->random(),
            'title' => 'Technical Lead',
            'permissions_level' => 'Owner'
        ];
    }
}
