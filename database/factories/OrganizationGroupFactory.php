<?php

namespace Database\Factories;

use App\Models\Organization;
use App\Models\OrganizationGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationGroupFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrganizationGroup::class;

    public function definition()
    {
        return [
            'organisation_id' => Organization::pluck('id')->random(),
            'name' => 'Group 1'
        ];
    }
}
