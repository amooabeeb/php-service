<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\UserLinkedin;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserLinkedinFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserLinkedin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::pluck('id')->random(),
            'socialite_id' => $this->faker->randomKey(),
            'nickname' => $this->faker->name,
            'email_address' => $this->faker->unique()->safeEmail,
            'last_name' => $this->faker->lastName,
            'first_name' => $this->faker->firstName
        ];
    }
}
