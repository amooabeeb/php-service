<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group')) {
            Schema::create('group', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('organisation_id');
                $table->string('name');
                $table->timestamps();
                $table->softDeletes();

            });
        }

        if (!Schema::hasTable('user_group')) {
            Schema::create('user_group', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('group_id');
                $table->integer('user_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group');
        Schema::dropIfExists('user_group');
    }
}
