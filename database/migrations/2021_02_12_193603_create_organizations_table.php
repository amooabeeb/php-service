<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    private string $tableName = 'organisation';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->id();
                $table->string('organisation')->nullable();
                $table->string('website_url')->nullable();
                $table->string('domain')->nullable();
                $table->string('sub_domain')->nullable();
                $table->string('size_employees')->nullable();
                $table->boolean('has_paid')->default(false);
                $table->boolean('collect_candidate_pii')->default(false);
                $table->integer('is_recruitment_company')->default(0);
                $table->unsignedBigInteger('user_id')->index()->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->unsignedBigInteger('industry_id')->index()->nullable();
                $table->foreign('industry_id')->references('id')->on('industry')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
