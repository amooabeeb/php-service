<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLinkedin extends Model
{
    use HasFactory;

    /**
     * The table name in the database
     *
     * @var string
     */

    protected $table = 'user_linkedin';
}
