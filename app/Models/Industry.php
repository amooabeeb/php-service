<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'industry', 'industry_code', 'industry_group',
    ];

    /**
     * The table name in the database
     *
     * @var string
     */

    protected $table = 'industry';
}
