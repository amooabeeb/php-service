<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

class User extends BaseModel
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'name', 'email', 'activation_code', 'persist_code', 'permissions',
        'is_activated', 'activated_at', 'surname', 'is_guest', 'is_superuser',
        'first_name', 'last_name', 'unsubscribe_code', 'unsubscribed', 'feedback_request',
        'is_organisation', 'deleted_by', 'user_type', 'phone_number', 'invite_code', 'user_timezone'
    ];

    /**
     * The attributes excluded from the model's JSON.
     *
     * @var string[]
     */
    protected $hidden = ['password', 'activation_code', 'reset_password_code', 'pivot'];

    protected $appends = ['user_permissions'];

    /**
     * Recruiter detail belonging to a user
     *
     * @return HasOne
     */
    public function recruiterDetail(): HasOne
    {
        return $this->hasOne(RecruiterUserDetail::class);
    }

    /**
     * Recruiter detail belonging to a user
     *
     * @return HasOne
     */
    public function curatorDetail(): HasOne
    {
        return $this->hasOne(CuratorUserDetail::class);
    }

    /**
     * Get the user_permissions attribute
     *
     * @return mixed |null
     */
    public function getUserPermissionsAttribute()
    {
        return $this->getPermission();
    }

    /**
     * Get permissions field of a user
     *
     * @return mixed |null
     */
    public function getPermission()
    {
        switch (strtolower($this->user_type)) {
            case 'curator':
                return $this->curatorDetail ? $this->curatorDetail->roles_csv : null;
            case 'recruiter':
                return $this->recruiterDetail ? $this->recruiterDetail->permissions_level : null;
            default:
                return null;
        }
    }

    /**
     * Scope profile query based on user
     *
     * @param $query
     *
     * @return Builder |null
     */
    public function scopeWithProfileDetail($query)
    {
        switch (strtolower($this->user_type)) {
            case 'curator':
                return $query->with('curatorDetail');
            case 'recruiter':
                return $query->with('recruiterDetail');
            default:
                return null;
        }
    }

    /**
     * Organization owned by user
     *
     * @return HasOne
     */
    public function organization(): HasOne
    {
        return $this->hasOne(Organization::class);
    }
}
