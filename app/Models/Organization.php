<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Organization extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'organisation', 'user_id', 'domain', 'website_url', 'subdomain', 'industry_id', 'size_employees',
        'is_recruitment_company', 'has_paid'
    ];

    /**
     * The table name in the database
     *
     * @var string
     */

    protected $table = 'organisation';

    /**
     * Industry to which an organization belongs o
     *
     * @return BelongsTo
     */
    public function industry(): BelongsTo
    {
        return $this->belongsTo(Industry::class);
    }

    /**
     * Recruiters that belong to an organization
     *
     * @return HasMany
     */
    public function recruiters(): HasMany
    {
        return $this->hasMany(RecruiterUserDetail::class);
    }

    /**
     *  User that created organization (typically has owner attribute)
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
