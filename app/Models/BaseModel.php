<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    use HasFactory;

    /**
     * The attributes excluded from the model's JSON.
     *
     * @var string[]
     */
    protected $hidden = ['pivot'];
}
