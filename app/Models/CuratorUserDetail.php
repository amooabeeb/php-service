<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CuratorUserDetail extends Model
{
    use HasFactory;

    /**
     * The table name in the database
     *
     * @var string
     */

    protected $table = 'curator_user_detail';

    /**
     * User to which recruiter profile belongs to
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
