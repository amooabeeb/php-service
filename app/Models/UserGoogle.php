<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGoogle extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'user_id', 'token', 'socialite_id'
    ];

    /**
     * The table name in the database
     *
     * @var string
     */

    protected $table = 'user_google';
}
