<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class GroupService
{
    /**
     * Check if group has active assessment
     *
     * @param int    $groupId
     * @param string $clientId
     * @param string $token
     *
     * @return bool
     */
    public static function hasActiveAssessment(int $groupId, string $clientId, string $token): bool
    {
        $baseUrl = config('internal.api_gateway.base_url');
        $url = "$baseUrl/v1/groups/$groupId/has-assessment?client_id=$clientId";
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json'
        ])->get($url);
        $responseBody = $response->json();
        if ($responseBody && isset($responseBody['has_assessment'])) {
            return $responseBody['has_assessment'];
        }
        return true;
    }
}
