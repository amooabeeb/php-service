<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class AuthService
{
    /**
     * Get JWT key from auth service
     *
     * @param string $clientId
     *
     * @return string|null
     */
    public static function getJWTKey(string $clientId): ?string
    {
        $authUrl = config('internal.auth.base_url');
        $url = "$authUrl/v1/key?client_id=$clientId";
        $response = Http::get($url);
        $responseBody = $response->json();
        if ($responseBody && $responseBody['key']) {
            return $responseBody['key'];
        }
        return null;
    }
}
