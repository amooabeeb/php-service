<?php

namespace App\Services;

use DateTimeZone;
use Illuminate\Support\Facades\Cache;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\ValidAt;

class JwtService
{

    /**
     * Initialize JWT configuration
     *
     * @param Key $jwtKey
     *
     * @return Configuration
     */
    public static function getJWTConfiguration(Key $jwtKey): Configuration
    {
        $jwtConfiguration = Configuration::forSymmetricSigner(
            new Sha256(),
            $jwtKey
        );

        $jwtConfiguration->setValidationConstraints(
            new ValidAt(new SystemClock(new DateTimeZone(\date_default_timezone_get()))),
            new SignedWith(new Sha256(), $jwtKey),
        );
        return $jwtConfiguration;
    }

    /**
     * Get JWTKey from cache or auth service
     *
     * @param string $clientId
     *
     * @return Key|null
     */
    public static function getJwtKey(string $clientId): ?Key
    {
        $jwtKey = Cache::get('jwt_key');
        if ($jwtKey) {
            return $jwtKey;
        }
        $jwtKey = AuthService::getJWTKey($clientId);
        if (!$jwtKey) {
            return null;
        }
        $jwtKey = InMemory::base64Encoded($jwtKey);
        Cache::put('jwt_key', $jwtKey);
        return $jwtKey;
    }
}
