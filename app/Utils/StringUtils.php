<?php

namespace App\Utils;

class StringUtils
{

    /**
     * Checks if a string is in json format
     *
     * @param string $text
     *
     * @return bool
     */
    public static function isJson(string $text): bool
    {
        return is_array(json_decode($text, true));
    }
}
