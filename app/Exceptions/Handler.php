<?php

namespace App\Exceptions;

use App\Common\Errors\CustomError;
use App\Traits\JSONResponse;
use Exception;
use HttpRequestMethodException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Throwable;

class Handler extends ExceptionHandler
{
    use JSONResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //Log::error($e);
            error_log('PROFILE ERROR LOG: ' . $e);
        });

        $this->renderable(function (NotFoundHttpException $e) {
            return $this->sendClientErrorResponse([CustomError::ROUTE_NOT_FOUND], 404);
        });

        $this->renderable(function (MethodNotAllowedHttpException $e) {
            return $this->sendClientErrorResponse([CustomError::METHOD_NOT_SUPPORTED], 405);
        });

        if (config('app.env') === 'production') {
            $this->renderable(function (Throwable $e) {
                return $this->sendServerErrorResponse($e, 500);
            });
        }
    }
}
