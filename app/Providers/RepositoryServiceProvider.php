<?php

namespace App\Providers;

use App\Repositories\Concrete\OrganizationGroupRepository;
use App\Repositories\Concrete\OrganizationRepository;
use App\Repositories\Concrete\RecruiterDetailRepository;
use App\Repositories\Concrete\SocialAuthRepository;
use App\Repositories\Concrete\UserRepository;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use App\Repositories\Contracts\OrganizationRepositoryInterface;
use App\Repositories\Contracts\RecruiterDetailRepositoryInterface;
use App\Repositories\Contracts\SocialAuthRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OrganizationRepositoryInterface::class, OrganizationRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(RecruiterDetailRepositoryInterface::class, RecruiterDetailRepository::class);
        $this->app->bind(SocialAuthRepositoryInterface::class, SocialAuthRepository::class);
        $this->app->bind(OrganizationGroupRepositoryInterface::class, OrganizationGroupRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
