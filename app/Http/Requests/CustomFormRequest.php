<?php

namespace App\Http\Requests;

use App\Traits\JSONResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Ignore custom form requests
 *
 * @codeCoverageIgnore
 */

abstract class CustomFormRequest extends FormRequest
{
    use JSONResponse;

    /**
     * Override failedValidation method when request fails validation
     *
     * @param Validator $validator
     *
     * @throws HttpResponseException
     *
     * @return void
     */
    public function failedValidation(Validator $validator)
    {
        $validationErrors = $this->getErrorMessages($validator->errors()->messages());
        throw new HttpResponseException(
            $this->sendClientErrorResponse($validationErrors, 422)
        );
    }

    /**
     * Format laravel validation errors
     *
     * @param array $errors
     *
     * @return array
     */
    public function getErrorMessages(array $errors): array
    {
        $result = [];
        foreach ($errors as $key => $fieldErrors) {
            foreach ($fieldErrors as $fieldError) {
                array_push($result, ['title' => $key, 'detail' => $fieldError]);
            }
        }
        return $result;
    }
}
