<?php

namespace App\Http\Requests;

/**
 * Ignore custom form requests
 *
 * @codeCoverageIgnore
 */

class UpdateUserRequest extends CustomFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_timezone' => 'string',
            'first_name' => 'string|max:190',
            'last_name' => 'string'
        ];
    }
}
