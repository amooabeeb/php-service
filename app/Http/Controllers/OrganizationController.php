<?php

namespace App\Http\Controllers;

use App\Common\Errors\OrganizationError;
use App\Repositories\Contracts\OrganizationRepositoryInterface;
use App\Repositories\Contracts\RecruiterDetailRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{

    private OrganizationRepositoryInterface $organizationRepository;

    private RecruiterDetailRepositoryInterface $recruiterDetailRepository;

    private UserRepositoryInterface $userRepository;

    /**
     * OrganizationController constructor.
     *
     * @param OrganizationRepositoryInterface    $organizationRepository
     * @param UserRepositoryInterface            $userRepository
     * @param RecruiterDetailRepositoryInterface $recruiterDetailRepository
     */
    public function __construct(
        OrganizationRepositoryInterface $organizationRepository,
        UserRepositoryInterface $userRepository,
        RecruiterDetailRepositoryInterface $recruiterDetailRepository
    ) {
        $this->organizationRepository = $organizationRepository;
        $this->userRepository = $userRepository;
        $this->recruiterDetailRepository = $recruiterDetailRepository;
    }

    /**
     * Get organization details from repository
     *
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function getOrganizationDetails(Request $request, int $id): JsonResponse
    {
        $userAuth = $request->auth;
        $organization = $this->organizationRepository->getByIdWithIndustry($id);
        if (!$organization) {
            return $this->sendClientErrorResponse([OrganizationError::ORGANIZATION_NOT_FOUND], 400);
        }
        $recruiters = $this->recruiterDetailRepository->getRecruitersForOrganization($id);
        if (!$this->recruiterDetailRepository->canAccessOrganization($recruiters, $userAuth['userId'])) {
            return $this->sendClientErrorResponse([OrganizationError::FORBIDDEN_ACCESS], 403);
        }
        return $this->sendSuccessResponse($organization);
    }

    /**
     * Get users for an organization
     *
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function getOrganizationUsers(Request $request, int $id): JsonResponse
    {
        $userAuth = $request->auth;
        $organization = $this->organizationRepository->getById($id);
        if (!$organization) {
            return $this->sendClientErrorResponse([OrganizationError::ORGANIZATION_NOT_FOUND], 400);
        }
        $recruiters = $this->recruiterDetailRepository->getRecruitersForOrganization($id);
        if (!$this->recruiterDetailRepository->canAccessOrganization($recruiters, $userAuth['userId'])) {
            return $this->sendClientErrorResponse([OrganizationError::FORBIDDEN_ACCESS], 403);
        }
        $organizationUsers = $this->userRepository->getOrganizationUsers($id);
        return $this->sendSuccessResponse($organizationUsers);
    }
}
