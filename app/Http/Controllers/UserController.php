<?php

namespace App\Http\Controllers;

use App\Common\Errors\UserError;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use App\Repositories\Contracts\SocialAuthRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Utils\StringUtils;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{

    private UserRepositoryInterface $userRepository;
    private SocialAuthRepositoryInterface $socialAuthRepository;
    private OrganizationGroupRepositoryInterface $organizationGroupRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepositoryInterface              $userRepository
     * @param SocialAuthRepositoryInterface        $socialAuthRepository
     * @param OrganizationGroupRepositoryInterface $organizationGroupRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        SocialAuthRepositoryInterface $socialAuthRepository,
        OrganizationGroupRepositoryInterface $organizationGroupRepository
    ) {
        $this->userRepository = $userRepository;
        $this->socialAuthRepository = $socialAuthRepository;
        $this->organizationGroupRepository = $organizationGroupRepository;
    }

    /**
     * Fetch user's details from repository
     *
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function getUserDetails(Request $request, int $id): JsonResponse
    {
        $userAuth = $request->auth;
        if (!$this->userRepository->canAccessUserDetails($userAuth['userId'], $id)) {
            return $this->sendClientErrorResponse([UserError::FORBIDDEN_ACCESS], 403);
        }
        $user = $this->userRepository->getById($id);
        return $this->sendSuccessResponse($user);
    }

    /**
     * Get all available timezones
     *
     * @return JsonResponse
     */
    public function getAllTimezones(): JsonResponse
    {
        $timezones = $this->userRepository->getAllTimezones();
        return $this->sendSuccessResponse($timezones);
    }

    /**
     * Update user profile
     *
     * @param UpdateUserRequest $request
     * @param int               $id
     *
     * @return JsonResponse
     */
    public function updateProfile(UpdateUserRequest $request, int $id): JsonResponse
    {
        $updateData = $request->validated();
        $userAuth = $request->auth;
        if (!$this->userRepository->canAccessUserDetails($userAuth['userId'], $id)) {
            return $this->sendClientErrorResponse([UserError::FORBIDDEN_ACCESS], 403);
        }
        $updatedUser = $this->userRepository->updateUserDetails($id, $updateData);
        return $this->sendSuccessResponse($updatedUser, 200);
    }

    /**
     * Format user's permissions to an array format
     *
     * @param string $permissions
     *
     * @return mixed|string[]
     */
    public function formatUserPermissions(string $permissions)
    {
        return StringUtils::isJson($permissions) ? json_decode($permissions) : [$permissions];
    }

    /**
     * Update user profile
     *
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function getUserPermissions(Request $request, int $id): JsonResponse
    {
        $user = $this->userRepository->getByIdWithProfile($id);
        $responseData = [
            'user_type' => $user->user_type,
            'user_permissions' => $user->user_permissions ? $this->formatUserPermissions($user->user_permissions) : null
        ];
        return $this->sendSuccessResponse($responseData);
    }

    /**
     * Get user social auth profile
     *
     * @param Request $request
     * @param string  $driver
     * @param string  $socialiteId
     *
     * @return JsonResponse
     */
    public function getSocialAuthProfile(Request $request, string $driver, string $socialiteId): JsonResponse
    {
        $socialProfile = $this->socialAuthRepository->getSocialAuthBySocialiteId($socialiteId, $driver);
        if (!$socialProfile) {
            return $this->sendClientErrorResponse([UserError::USER_NOT_FOUND], 422);
        }
        return $this->sendSuccessResponse($socialProfile);
    }

    /**
     * Get user groups
     *
     * @param Request $request
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function getUserGroups(Request $request, int $id): JsonResponse
    {
        $user = $this->userRepository->getByIdWithProfile($id);
        $organizationGroups = $this->organizationGroupRepository->getUserGroups($id);
        $response = [
            'organisation_id' => $user->recruiterDetail->organisation_id,
            'groups' => $organizationGroups
        ];
        return $this->sendSuccessResponse($response);
    }
}
