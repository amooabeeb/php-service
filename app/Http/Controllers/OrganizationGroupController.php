<?php

namespace App\Http\Controllers;

use App\Common\Errors\OrganizationError;
use App\Http\Requests\SyncUserToGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\GroupService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrganizationGroupController extends Controller
{

    private OrganizationGroupRepositoryInterface $organizationGroupRepository;

    private UserRepositoryInterface $userRepository;

    /**
     * OrganizationGroupController constructor.
     *
     * @param OrganizationGroupRepositoryInterface $organizationGroupRepository
     * @param UserRepositoryInterface              $userRepository
     */
    public function __construct(
        OrganizationGroupRepositoryInterface $organizationGroupRepository,
        UserRepositoryInterface $userRepository
    ) {
        $this->organizationGroupRepository = $organizationGroupRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Get recruiter's organization id from request
     *
     * @param Request $request
     *
     * @return mixed
     */
    private function getOrganizationIdFromRequest(Request $request)
    {
        $recruiter = $request->user;
        return $recruiter->recruiterDetail->organisation_id;
    }

    /**
     * Create a group
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createGroup(Request $request): JsonResponse
    {
        $organizationId = $this->getOrganizationIdFromRequest($request);
        $this->organizationGroupRepository->createGroup($organizationId);
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId($organizationId);
        return $this->sendSuccessResponse($groups);
    }

    /**
     * Get all groups that belong to an organization
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAllGroupsForAnOrganization(Request $request): JsonResponse
    {
        $organizationId = $this->getOrganizationIdFromRequest($request);
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId($organizationId);
        return $this->sendSuccessResponse($groups);
    }

    /**
     * Add user to a group
     *
     * @param SyncUserToGroupRequest $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function addUserToGroup(SyncUserToGroupRequest $request, $id): JsonResponse
    {
        $organizationId = $this->getOrganizationIdFromRequest($request);
        $userAdded = $this->organizationGroupRepository->addUserToGroup($id, $request->user_id);
        if (!$userAdded) {
            return $this->sendClientErrorResponse([OrganizationError::ADD_USER_TO_GROUP]);
        }
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId($organizationId);
        return $this->sendSuccessResponse($groups);
    }

    /**
     * Delete user from a group
     *
     * @param Request $request
     * @param int     $id
     * @param int     $userId
     *
     * @return JsonResponse
     */
    public function detachUserFromGroup(Request $request, int $id, int $userId): JsonResponse
    {
        $organizationId = $this->getOrganizationIdFromRequest($request);
        $userDetached = $this->organizationGroupRepository->deleteUserFromGroup($id, $userId);
        if (!$userDetached) {
            return $this->sendClientErrorResponse([OrganizationError::DETACH_USER_FROM_GROUP]);
        }
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId($organizationId);
        return $this->sendSuccessResponse($groups);
    }

    /**
     * Update a group together with members
     *
     * @param UpdateGroupRequest $request
     * @param int                $id
     *
     * @return JsonResponse
     */
    public function updateGroup(UpdateGroupRequest $request, int $id): JsonResponse
    {
        $requestData = $request->validated();
        $organizationId = $this->getOrganizationIdFromRequest($request);
        if ($this->organizationGroupRepository->checkGroupExists($requestData['name'], $organizationId, $id)) {
            return $this->sendClientErrorResponse([OrganizationError::UNIQUE_GROUP_NAME]);
        }
        $this->organizationGroupRepository->updateGroupWithUsers($id, $requestData);
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId($organizationId);
        return $this->sendSuccessResponse($groups);
    }

    /**
     * Delete a group
     *
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function deleteGroup(Request $request, int $id): JsonResponse
    {
        $token = $request->bearerToken();
        $clientId = $request->input('client_id');
        if (GroupService::hasActiveAssessment($id, $clientId, $token)) {
            return $this->sendClientErrorResponse([OrganizationError::GROUP_HAS_ASSESSMENT], 400);
        }
        $organizationId = $this->getOrganizationIdFromRequest($request);
        $this->organizationGroupRepository->deleteGroup($id);
        $groups = $this->organizationGroupRepository->getGroupsByOrganizationId($organizationId);
        return $this->sendSuccessResponse($groups);
    }
}
