<?php

namespace App\Http\Middleware;

use App\Common\Errors\UserError;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\JSONResponse;
use Closure;
use Illuminate\Http\Request;

class CheckUserExistence
{
    use JSONResponse;

    private UserRepositoryInterface $userRepository;

    /**
     * CheckUserExistence constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $userId = $request->route('id');
        $user = $this->userRepository->getById($userId);
        if (!$user) {
            return $this->sendClientErrorResponse([UserError::USER_NOT_FOUND], 400);
        }
        return $next($request);
    }
}
