<?php

namespace App\Http\Middleware;

use App\Common\Errors\CustomError;
use App\Common\Errors\OrganizationError;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\JSONResponse;
use Closure;
use Illuminate\Http\Request;

class ValidateGroupAccess
{
    use JSONResponse;

    private UserRepositoryInterface $userRepository;

    private OrganizationGroupRepositoryInterface $organizationGroupRepository;

    /**
     * ValidateGroupAccess constructor.
     *
     * @param UserRepositoryInterface              $userRepository
     * @param OrganizationGroupRepositoryInterface $organizationGroupRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        OrganizationGroupRepositoryInterface $organizationGroupRepository
    ) {
        $this->userRepository = $userRepository;
        $this->organizationGroupRepository = $organizationGroupRepository;
    }
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $allowedRoles = ['admin', 'owner'];
        $userAuth = $request->auth;
        $userId = $userAuth['userId'];
        $user = $this->userRepository->getByIdWithProfile($userId);

        if ($user->user_type !== 'recruiter' || !in_array(strtolower($user->user_permissions), $allowedRoles)) {
            return $this->sendClientErrorResponse([CustomError::FORBIDDEN_ACCESS], 403);
        }

        $groupId = $request->route('id');
        if ($groupId) {
            $group = $this->organizationGroupRepository->getGroupById($groupId);
            if (!$group) {
                return $this->sendClientErrorResponse([OrganizationError::GROUP_NOT_FOUND], 400);
            } elseif ($group->organisation_id !== $user->recruiterDetail->organisation_id) {
                return $this->sendClientErrorResponse([CustomError::FORBIDDEN_ACCESS], 403);
            }
        }

        $request->merge(['user' => $user]);
        return $next($request);
    }
}
