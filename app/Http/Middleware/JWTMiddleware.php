<?php

namespace App\Http\Middleware;

use App\Common\Errors\CustomError;
use App\Services\JwtService;
use App\Traits\JSONResponse;
use Closure;
use Illuminate\Http\Request;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Encoding\CannotDecodeContent;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token\InvalidTokenStructure;
use Lcobucci\JWT\Token\UnsupportedHeaderFound;
use Lcobucci\JWT\Validation\RequiredConstraintsViolated;

class JWTMiddleware
{
    use JSONResponse;

    /**
     * Instance of a JWT Configuration
     *
     * @var Configuration
     */
    private Configuration $jwtConfiguration;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        $clientId = $request->input('client_id');

        if (!$token) {
            return $this->sendClientErrorResponse([CustomError::NO_TOKEN], 401);
        } elseif (!$clientId) {
            return $this->sendClientErrorResponse([CustomError::NO_CLIENT_ID], 400);
        }

        $jwtKey = JwtService::getJwtKey($clientId);

        if (!$jwtKey) {
            return $this->sendClientErrorResponse([CustomError::NO_JWT_KEY], 401);
        }

        $this->setJwtConfiguration($jwtKey);

        try {
            // Attempt to parse and validate the JWT
            $decodedToken = $this->jwtConfiguration->parser()->parse($token);
            $constraints = $this->jwtConfiguration->validationConstraints();
            $this->jwtConfiguration->validator()->assert($decodedToken, ...$constraints);
            $request->merge(['auth' => ['userId' => $decodedToken->claims()->get('ufid')]]);
            return $next($request);
        } catch (RequiredConstraintsViolated $exception) {
            return $this->sendClientErrorResponse([CustomError::EXPIRED_TOKEN], 401);
        } catch (CannotDecodeContent | InvalidTokenStructure | UnsupportedHeaderFound $exception) {
            return $this->sendClientErrorResponse([CustomError::INVALID_TOKEN], 401);
        }
    }

    /**
     * Set JWT configuration
     *
     * @param Key $jwtKey
     *
     * @return void
     */
    private function setJwtConfiguration(Key $jwtKey): void
    {
        $this->jwtConfiguration = JwtService::getJWTConfiguration($jwtKey);
    }
}
