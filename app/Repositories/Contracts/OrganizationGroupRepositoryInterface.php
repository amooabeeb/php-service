<?php

namespace App\Repositories\Contracts;

use App\Models\OrganizationGroup;
use Illuminate\Support\Collection;

interface OrganizationGroupRepositoryInterface
{

    /**
     * Get group by Id
     *
     * @param int $id
     *
     * @return OrganizationGroup|null
     */
    public function getGroupById(int $id): ?OrganizationGroup;

    /**
     * Check if group exists already
     *
     * @param string   $name
     * @param int|null $organizationId
     * @param int|null $groupId
     *
     * @return bool
     */
    public function checkGroupExists(string $name, int $organizationId, int $groupId = null): bool;

    /**
     * Get groups in an organization
     *
     * @param int $organizationId
     *
     * @return Collection
     */
    public function getGroupsByOrganizationId(int $organizationId): Collection;

    /**
     * Add a user to a group
     *
     * @param int $groupId
     * @param int $userId
     *
     * @return bool
     */
    public function addUserToGroup(int $groupId, int $userId): bool;

    /**
     * Remove a user from a group
     *
     * @param int $groupId
     * @param int $userId
     *
     * @return bool
     */
    public function deleteUserFromGroup(int $groupId, int $userId): bool;

    /**
     * Update group together with its users
     *
     * @param int   $groupId
     * @param array $data
     *
     * @return OrganizationGroup
     */
    public function updateGroupWithUsers(int $groupId, array $data): OrganizationGroup;

    /**
     * Create a group
     *
     * @param int $organizationId
     *
     * @return OrganizationGroup
     */
    public function createGroup(int $organizationId): OrganizationGroup;

    /**
     * Delete a group
     *
     * @param int $groupId
     *
     * @return bool
     */
    public function deleteGroup(int $groupId): bool;

    /**
     * Get the organization groups to which a user belongs
     *
     * @param int $userId
     *
     * @return Collection<OrganizationGroup>
     */
    public function getUserGroups(int $userId): Collection;
}
