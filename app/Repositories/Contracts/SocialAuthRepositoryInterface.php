<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface SocialAuthRepositoryInterface
{
    /**
     * Return social auth data based on driver and socialiteId
     *
     * @param string $socialiteId
     * @param string $driver
     *
     * @return Model|null
     */
    public function getSocialAuthBySocialiteId(string $socialiteId, string $driver): ?Model;
}
