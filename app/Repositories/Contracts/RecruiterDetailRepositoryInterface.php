<?php

namespace App\Repositories\Contracts;

use App\Models\RecruiterUserDetail;
use Illuminate\Database\Eloquent\Collection;

interface RecruiterDetailRepositoryInterface
{
    /**
     * Return a collection of recruiters for an organization
     *
     * @param int $organizationId
     *
     * @return Collection<RecruiterUserDetail>
     */
    public function getRecruitersForOrganization(int $organizationId): Collection;

    /**
     * Determine if a user can access an organization or not
     *
     * @param Collection $recruiters
     * @param int        $requesterId
     *
     * @return bool
     */
    public function canAccessOrganization(Collection $recruiters, int $requesterId): bool;
}
