<?php

namespace App\Repositories\Contracts;

use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use Illuminate\Database\Eloquent\Collection;

interface OrganizationRepositoryInterface
{
    /**
     * Get an organization by Id
     *
     * @param int $id
     *
     * @return Organization|null
     */
    public function getById(int $id): ?Organization;

    /**
     * Get an organization by Id with industry info
     *
     * @param int $id
     *
     * @return Organization|null
     */
    public function getByIdWithIndustry(int $id): ?Organization;
}
