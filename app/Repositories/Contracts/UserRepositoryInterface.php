<?php

namespace App\Repositories\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    /**
     * Get a user by Id
     *
     * @param int $id
     *
     * @return User|null
     */
    public function getById(int $id): ?User;

    /**
     * Get all timezones
     *
     * @return array
     */
    public function getAllTimezones(): array;

    /**
     * Determine if a requester can view user details
     *
     * @param int $requesterId
     * @param int $userId
     *
     * @return bool
     */
    public function canAccessUserDetails(int $requesterId, int $userId): bool;

    /**
     * Update a user's details
     *
     * @param int   $userId
     * @param array $updateData
     *
     * @return User
     */
    public function updateUserDetails(int $userId, array $updateData): User;

    /**
     * Get Organization
     *
     * @param int $organizationId
     *
     * @return Collection
     */
    public function getOrganizationUsers(int $organizationId): Collection;

    /**
     * Get a user by Id
     *
     * @param int $id
     *
     * @return User|null
     */
    public function getByIdWithProfile(int $id): ?User;
}
