<?php

namespace App\Repositories\Concrete;

use App\Models\UserGoogle;
use App\Models\UserLinkedin;
use App\Repositories\Contracts\SocialAuthRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class SocialAuthRepository implements SocialAuthRepositoryInterface
{

    protected UserGoogle $userGoogle;
    protected UserLinkedin $userLinkedin;

    /**
     * SocialAuthRepository constructor.
     *
     * @param UserGoogle   $userGoogle
     * @param UserLinkedin $userLinkedin
     */
    public function __construct(UserGoogle $userGoogle, UserLinkedin $userLinkedin)
    {
        $this->userGoogle = $userGoogle;
        $this->userLinkedin = $userLinkedin;
    }

    /**
     * Get the appropriate model for driver
     *
     * @param string $driver
     *
     * @return UserGoogle|UserLinkedin
     */
    public function getDriver(string $driver)
    {
        switch (strtolower($driver)) {
            case 'google':
                return $this->userGoogle;
            default:
                return $this->userLinkedin;
        }
    }

    /**
     * Return social auth data based on driver and socialiteId
     *
     * @param string $socialiteId
     * @param string $driver
     *
     * @return Model|null
     */
    public function getSocialAuthBySocialiteId(string $socialiteId, string $driver): ?Model
    {
        $model = $this->getDriver($driver);
        return $model->where('socialite_id', $socialiteId)->first();
    }
}
