<?php

namespace App\Repositories\Concrete;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements UserRepositoryInterface
{

    private User $user;

    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user by Id
     *
     * @param int $id
     *
     * @return User
     */
    public function getById(int $id): ?User
    {
        return $this->user->where('id', $id)->with('recruiterDetail')->first();
    }

    /**
     * Returns a list of all timezones e.g Africa/Lagos
     *
     * @return array
     * @throws \Exception
     */
    public function getAllTimezones(): array
    {
        $selectOptions = [];
        foreach (DateTimeZone::listIdentifiers() as $zoneLabel) {
            $currentTimeInZone = new DateTime('now', new DateTimeZone($zoneLabel));
            $currentTimeDiff = $currentTimeInZone->format('P');
            $selectOptions[] = [
                'offset' => $currentTimeDiff,
                'code' => $zoneLabel
            ];
        }
        return $selectOptions;
    }

    /**
     * Determine if a requester can view user details
     *
     * @param int $requesterId
     * @param int $userId
     *
     * @return bool
     */
    public function canAccessUserDetails(int $requesterId, int $userId): bool
    {
        return $requesterId == $userId;
    }

    /**
     * Update a user's details
     *
     * @param int   $userId
     * @param array $updateData
     *
     * @return User
     */
    public function updateUserDetails(int $userId, array $updateData): User
    {
        $this->user->where('id', $userId)->update($updateData);
        return $this->getById($userId);
    }

    /**
     * Get all users associated with an organization
     *
     * @param int $organizationId
     *
     * @return Collection
     */
    public function getOrganizationUsers(int $organizationId): Collection
    {
        return $this->user->whereHas('recruiterDetail', function ($query) use ($organizationId) {
            $query->where('organisation_id', $organizationId);
        })->with('recruiterDetail')->get();
    }

    /**
     *  Get Id of a user together with the profile
     *
     * @param int $id
     *
     * @return User|null
     */
    public function getByIdWithProfile(int $id): ?User
    {
        return $this->user->where('id', $id)->withProfileDetail()->first();
    }
}
