<?php

namespace App\Repositories\Concrete;

use App\Models\OrganizationGroup;
use App\Repositories\Contracts\OrganizationGroupRepositoryInterface;
use Exception;
use Illuminate\Support\Collection;

class OrganizationGroupRepository implements OrganizationGroupRepositoryInterface
{

    private OrganizationGroup $organizationGroup;

    /**
     * OrganizationGroupRepository constructor.
     *
     * @param OrganizationGroup $organizationGroup
     */
    public function __construct(OrganizationGroup $organizationGroup)
    {
        $this->organizationGroup = $organizationGroup;
    }

    /**
     * Get group by Id
     *
     * @param int $id
     *
     * @return OrganizationGroup|null
     */
    public function getGroupById(int $id): ?OrganizationGroup
    {
        return $this->organizationGroup->find($id);
    }

    /**
     * Check if group exists already
     *
     * @param string   $name
     * @param int|null $organizationId
     * @param int|null $groupId
     *
     * @return bool
     */
    public function checkGroupExists(string $name, int $organizationId, int $groupId = null): bool
    {
        if (!$groupId) {
            return $this->organizationGroup->where(['organisation_id' => $organizationId, 'name' => $name])->exists();
        }
        $group = $this->organizationGroup->where(['organisation_id' => $organizationId, 'name' => $name])->first();
        if (!$group || ($groupId == $group->id)) {
            return false;
        }
        return true;
    }

    /**
     * Get groups in an organization
     *
     * @param int $organizationId
     *
     * @return Collection
     */
    public function getGroupsByOrganizationId(int $organizationId): Collection
    {
        $with = 'users:id,first_name,last_name,email';
        return $this->organizationGroup
            ->where('organisation_id', $organizationId)
            ->orderBy('created_at', 'desc')
            ->with($with)->get();
    }

    /**
     * Add a user to a group
     *
     * @param int $groupId
     * @param int $userId
     *
     * @return bool
     */
    public function addUserToGroup(int $groupId, int $userId): bool
    {
        $group = $this->organizationGroup->where('id', $groupId)->first();
        if ($group->users()->where('users.id', $userId)->exists()) {
            return true;
        }
        $group->users()->attach($userId);
        return true;
    }

    /**
     * Remove a user from a group
     *
     * @param int $groupId
     * @param int $userId
     *
     * @return bool
     */
    public function deleteUserFromGroup(int $groupId, int $userId): bool
    {
        return $this->organizationGroup->where('id', $groupId)->first()->users()->detach($userId);
    }

    /**
     * Update group together with its users
     *
     * @param int   $groupId
     * @param array $data
     *
     * @return OrganizationGroup
     */
    public function updateGroupWithUsers(int $groupId, array $data): OrganizationGroup
    {
        $group = $this->organizationGroup->where('id', $groupId)->first();
        $group->name = $data['name'];
        $group->save();
        return $group;
    }

    /**
     * Deduce the default group name
     *
     * @param Collection $pendingDefaultGroups
     *
     * @return string
     */
    private function deduceGroupName(Collection $pendingDefaultGroups): string
    {
        if ($pendingDefaultGroups->isEmpty()) {
            return 'Group 1';
        }
        $maxGroupNumber = 0;
        foreach ($pendingDefaultGroups as $group) {
            $groupDetails = explode(' ', $group->name);
            $groupNumber = intval($groupDetails[1]);
            if ($groupNumber > $maxGroupNumber) {
                $maxGroupNumber = $groupNumber;
            }
        }
        return "Group " . ($maxGroupNumber + 1);
    }

    /**
     * Create a group
     *
     * @param int $organizationId
     *
     * @return OrganizationGroup
     */
    public function createGroup(int $organizationId): OrganizationGroup
    {
        $pendingDefaultGroups = $this->organizationGroup
            ->where('organisation_id', $organizationId)
            ->where('name', 'like', '%Group %')->get();
        $groupName = $this->deduceGroupName($pendingDefaultGroups);
        return $this->organizationGroup->create(['organisation_id' => $organizationId, 'name' => $groupName]);
    }

    /**
     * Delete a group
     *
     * @param int $groupId
     *
     * @return bool
     */
    public function deleteGroup(int $groupId): bool
    {
        $group = $this->organizationGroup->where('id', $groupId)->first();
        $group->users()->detach();
        return $group->delete();
    }

    /**
     * Get the organizations groups to which a user belongs
     *
     * @param int $userId
     *
     * @return Collection<OrganizationGroup>
     */
    public function getUserGroups(int $userId): Collection
    {
        return $this->organizationGroup->whereHas('users', function ($query) use ($userId) {
            return $query->where('user_id', $userId);
        })->get();
    }
}
