<?php

namespace App\Repositories\Concrete;

use App\Models\Organization;
use App\Models\RecruiterUserDetail;
use App\Repositories\Contracts\OrganizationRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class OrganizationRepository implements OrganizationRepositoryInterface
{

    protected Organization $organization;

    /**
     * OrganizationRepository constructor.
     *
     * @param Organization $organization
     */
    public function __construct(Organization $organization)
    {
        $this->organization = $organization;
    }

    /**
     * Get organization by Id
     *
     * @param int $id
     *
     * @return Organization
     */
    public function getById(int $id): ?Organization
    {
        return $this->organization->find($id);
    }

    /**
     * Get organization by Id with industry
     *
     * @param int $id
     *
     * @return Organization|null
     */
    public function getByIdWithIndustry(int $id): ?Organization
    {
        return $this->organization->where('id', $id)->with('industry')->first();
    }
}
