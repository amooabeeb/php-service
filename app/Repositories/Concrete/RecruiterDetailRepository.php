<?php

namespace App\Repositories\Concrete;

use App\Models\RecruiterUserDetail;
use App\Repositories\Contracts\RecruiterDetailRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class RecruiterDetailRepository implements RecruiterDetailRepositoryInterface
{

    private RecruiterUserDetail $recruiterDetail;

    /**
     * RecruiterDetailRepository constructor.
     *
     * @param RecruiterUserDetail $recruiterDetail
     */
    public function __construct(RecruiterUserDetail $recruiterDetail)
    {
        $this->recruiterDetail = $recruiterDetail;
    }

    /**
     * Return a collection of recruiters for an organization
     *
     * @param int $organizationId
     *
     * @return Collection
     */
    public function getRecruitersForOrganization(int $organizationId): Collection
    {
        return $this->recruiterDetail->where('organisation_id', $organizationId)->get();
    }

    /**
     * Determine if a user can access an organization or not
     *
     * @param Collection $recruiters
     * @param int        $requesterId
     *
     * @return bool
     */
    public function canAccessOrganization(Collection $recruiters, int $requesterId): bool
    {
        foreach ($recruiters as $recruiter) {
            if ($recruiter->user_id == $requesterId) {
                return true;
            }
        }
        return false;
    }
}
