<?php

namespace App\Common\Errors;

class UserError
{

    public const FORBIDDEN_ACCESS = [
        'title' => 'Forbidden access to user',
        'detail' => 'Cannot access user\'s details'
    ];
    public const USER_NOT_FOUND = [
        'title' => 'User not found',
        'detail' => 'User does not exist'
    ];
}
