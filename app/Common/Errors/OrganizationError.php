<?php

namespace App\Common\Errors;

class OrganizationError
{
    public const ORGANIZATION_NOT_FOUND = [
        'title' => 'Organization not found',
        'detail' => 'Organization does not exist'
    ];
    public const GROUP_NOT_FOUND = [
        'title' => 'Group not found',
        'detail' => 'Group does not exist'
    ];
    public const FORBIDDEN_ACCESS = [
        'title' => 'Forbidden access to organization',
        'detail' => 'You are not allowed to access this organization\'s details'
    ];
    public const ADD_USER_TO_GROUP = [
        'title' => 'Unable to add user to group',
        'detail' => 'Unable to add user to group. Please verify that user and group exist'
    ];
    public const DETACH_USER_FROM_GROUP = [
        'title' => 'Unable to detach user from group',
        'detail' => 'Unable to detach user from group. Please verify that user and group exist'
    ];
    public const UPDATE_GROUP = [
        'title' => 'Unable to update group',
        'detail' => 'There was an error updating group details'
    ];
    public const CREATE_GROUP = [
        'title' => 'Unable to create group',
        'detail' => 'There was an error creating group'
    ];
    public const GROUP_HAS_ASSESSMENT = [
        'title' => 'Group has active assessment',
        'detail' => 'Unable to delete group. This group has one or more active assessments'
    ];
    public const UNIQUE_GROUP_NAME = [
        'title' => 'Group name exists',
        'detail' => 'Please provide a unique group name'
    ];
}
