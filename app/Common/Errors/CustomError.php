<?php

namespace App\Common\Errors;

class CustomError
{
    public const INVALID_TOKEN = [
        'title' => 'Invalid token',
        'detail' => 'Content of token is invalid or cannot be decoded'
    ];
    public const NO_TOKEN = [
        'title' => 'Token not provided',
        'detail' => 'Please include authorization token'
    ];
    public const EXPIRED_TOKEN = [
        'title' => 'Token invalid or expired',
        'detail' => 'The signature of the token could not be verified or token has expired'
    ];
    public const NO_CLIENT_ID = [
        'title' => 'No client Id provided',
        'detail' => 'Please provide client Id'
    ];
    public const NO_JWT_KEY = [
        'title' => 'No jwt key',
        'detail' => 'Unable to authenticate token'
    ];
    public const ROUTE_NOT_FOUND = [
        'title' => 'Route not found',
        'detail' => 'This route does not exist'
    ];
    public const METHOD_NOT_SUPPORTED = [
        'title' => 'Invalid HTTP method',
        'detail' => 'HTTP method not supported'
    ];
    public const FATAL_ERROR = [
        'title' => 'Fatal error',
        'detail' => 'Something went wrong'
    ];
    public const FORBIDDEN_ACCESS = [
        'title' => 'Forbidden access to this action',
        'detail' => 'You are not allowed to perform this action'
    ];
}
