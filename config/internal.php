<?php

return [
    // Configuration for internal services.

    'auth' => [
        'base_url' => env('AUTH_URL')
    ],
    'api_gateway' => [
        'base_url' => env('API_GATEWAY_URL')
    ]
];
